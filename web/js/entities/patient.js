/**
 * @file Patient js.
 */
(function ($) {
    /**
     * Patient.
     *
     * @returns {{init: Function, niceScroll: Function}}
     * @constructor
     */
    var Patient = function () {
        return {
            childFieldsMap: [
                {
                    fieldName: 'diseaseDate',
                    parentFieldObj: $('#appbundle_patient_filters_disease')
                },
                {
                    fieldName: 'heredityDisease',
                    parentFieldObj: $('#appbundle_patient_filters_heredity')
                },
                {
                    fieldName: 'examinationDate',
                    parentFieldObj: $('#appbundle_patient_filters_examination')
                },
                {
                    fieldName: 'pastIllnessesDate',
                    parentFieldObj: $('#appbundle_patient_filters_pastIllnesses')
                },
                {
                    fieldName: 'allergyDate',
                    parentFieldObj: $('#appbundle_patient_filters_allergy')
                }
            ],
            patientForm: $('form[name="appbundle_patient"]'),
            init: function () {
                var scope = this;

                // Process patient's form child fields.
                scope.processChild();

                // Prevent form submit by clicking enter and simulate tab click instead.
                scope.preventEnterFormSubmit();
            },
            preventEnterFormSubmit: function() {
                var scope = this;

                $('input', scope.patientForm).keydown(function (e) {
                    if (e.keyCode == 13) {
                        e.preventDefault();
                    }
                });
            },
            processChild: function () {
                var scope = this;

                // Child field appearance/disappearance.
                $.each(scope.childFieldsMap, function (i, v) {
                    var $thisChildFieldsBox = $('div.patient-child-fields-box[data-wrapper="' + v.fieldName + '"]'),
                        chooseCount = 0,
                        $thisBoxBody = $('.box-body', $thisChildFieldsBox);

                    v.parentFieldObj
                        // Selecting value.
                        .on('select2:select', function (e) {
                            var $thisField = $(this);

                            // Fetch html for new child field.
                            $.post(
                                Routing.generate('patient_child_field_markup'),
                                {
                                    'defaultValue': '',
                                    'parentValuesItem': e.params.data.id,
                                    'field': v.fieldName,
                                    'index': ++chooseCount
                                },
                                function (data) {
                                    // Append new field to the box.
                                    $thisBoxBody.append(data);

                                    // Show child fields box.
                                    $thisChildFieldsBox.removeClass('hidden');

                                    // Prevent form submit by clicking enter and simulate tab click instead.
                                    scope.preventEnterFormSubmit();
                                },
                                'html'
                            );
                        })
                        // Deselecting value.
                        .on('select2:unselect', function (e) {
                            var $thisField = $(this);

                            // Remove input.
                            $('input[data-item="' + e.params.data.id + '"]', $thisBoxBody).closest('.form-group').remove();

                            // Hide child fields box.
                            if (!$('option:selected', $thisField).length) {
                                $thisChildFieldsBox.addClass('hidden');
                                $thisBoxBody.html('');
                            }
                        });
                });

                // Submit child fields values.
                scope.patientForm.submit(function () {
                    $.each(scope.childFieldsMap, function (i, v) {
                        var fieldCommonVal = {};

                        // Collect all child fields values.
                        $('div[data-wrapper="' + v.fieldName + '"] input.child').each(function (i) {
                            var $thisInput = $(this),
                                $thisInputVal = $thisInput.val();

                            if ($thisInputVal.length) {
                                fieldCommonVal[$thisInput.data('item')] = $thisInputVal;
                            }
                        });

                        // Set json encoded values of child fields to the hidden entity field.
                        $('input[name="appbundle_patient[' + v.fieldName + ']"]').val(JSON.stringify(fieldCommonVal));
                    });
                });
            }
        };
    };

    // Dashboard init.
    $(document).ready(function () {
        window.Patient = new Patient();
        window.Patient.init();
    });
})(jQuery);