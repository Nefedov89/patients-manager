/**
 * @file Admin user js.
 */
(function ($) {

    var AdminUser = function () {
        return {
            init: function () {
                var scope = this;

                // Toggle user status.
                scope.toggleUserStatus();
            },

            // Toggle user status.
            toggleUserStatus: function () {
                var lockUserModal = $('#lockUserModal'),
                    lockUnlockUserMsgContainer = $('#message', lockUserModal);

                $('.user-status-modal').click(function () {
                    var $thisStatusLabel = $(this),
                        $thisUsername = $thisStatusLabel.data('username'),
                        $thisUid = $thisStatusLabel.data('uid');

                    // Show username in modal.
                    $('#username', lockUserModal).text($thisUsername);

                    // Clean up success msg after previous status update.
                    lockUnlockUserMsgContainer.attr('class', '').text('');

                    // Update status.
                    $('.user-status-toggle-btn', lockUserModal).unbind().click(function () {
                        var $thisModal = $(this),
                            $thisNewStatus = parseInt($thisModal.data('status'));

                        // Ajax call.
                        $.post(
                            Routing.generate('admin_toggle_user_status'),
                            {
                                uid: $thisUid,
                                newStatus: $thisNewStatus
                            },
                            function (data) {
                                var $lockStatus = $thisNewStatus ? 'unlock' : 'lock',
                                    $labelClasses = $thisNewStatus ? 'user-status-modal label label-success' : 'user-status-modal label label-danger',
                                    $labelText = $thisNewStatus ? Translator.trans('unlocked', {}, 'js') : Translator.trans('locked', {}, 'js');

                                // Show success message.
                                $('#message', lockUserModal)
                                    .addClass('alert alert-success alert-dismissible')
                                    .text(Translator.trans(
                                        'User %thisUsername% was successfully %labelText%',
                                        {
                                            'thisUsername': $thisUsername,
                                            'labelText': $labelText
                                        },
                                        'js'
                                    ));

                                // Change status label.
                                $thisStatusLabel.attr('class', $labelClasses);
                                $('i', $thisStatusLabel).attr('class', 'fa fa-fw fa-' + $lockStatus);
                                $('.status-text', $thisStatusLabel).text($labelText);
                            },
                            'json'
                        );
                    });
                });
            }
        };
    };

    // AdminUser init
    $(document).ready(function () {
        window.AdminUser = new AdminUser();
        window.AdminUser.init();
    });
})(jQuery);