/**
 * @file Global js.
 */
(function ($) {

    /**
     * App.
     *
     * @param options
     * @returns {{options, contactFormModal: (*|HTMLElement), contactForm: (*|HTMLElement), contactFormBtn: (*|HTMLElement), spinner: *, init: Function, contactFormProcess: Function, spinnerToggle: Function, select2: Function, navigation: Function, wysiwyg: Function, dataTables: Function}}
     * @constructor
     */
    var App = function (options) {
        return {
            options: $.extend(true, {}, options),

            contactFormModal: $(options.contactFormModal) || $('#contact-us-modal'),

            contactForm: $(options.contactForm) || $('#contact-us-form'),

            contactFormBtn: $(options.contactFormBtn) || $('#contact-us-modal button'),

            spinner: new Spinner({
                top: '60%',
                lines: 9,
                length: 8,
                width: 4,
                radius: 8
            }),

            init: function () {
                var scope = this;

                // Init nice scroll for all page.
                $('html').niceScroll();

                // Select 2.
                scope.select2();

                // Navigation.
                scope.navigation();

                // Wysiwyg.
                scope.wysiwyg();

                // Data tables.
                scope.dataTables();

                // Contact form.
                scope.contactFormProcess();

                // iCheck.
                scope.iCheck();
            },

            // Contact form.
            contactFormProcess: function () {
                var scope = this;

                scope.contactFormModal.on('show.bs.modal', function () {

                    // Ajax start/stop manipulations.
                    $(document)
                        .ajaxStart(function () {
                            // Enable spinner.
                            scope.spinnerToggle(true);

                            // Disable buttons.
                            scope.contactFormBtn.prop('disabled', true);
                        })
                        .ajaxStop(function () {
                            // Disable spinner.
                            scope.spinnerToggle(false);

                            // Enable buttons.
                            scope.contactFormBtn.prop('disabled', false);
                        });

                    // Ajax Contact form request.
                    scope.contactFormModal.on('submit', function (e) {
                        e.preventDefault();

                        // Hide errors messages.
                        scope.contactForm.find('div.field-error').addClass('hidden');

                        $.ajax({
                            method: "POST",
                            url: Routing.generate('contact_us'),
                            data: {
                                'formData': {
                                    'email': scope.contactForm.find('input#email').val(),
                                    'message': scope.contactForm.find('textarea#message').val()
                                }
                            },
                            success: function (data) {
                                window.location.reload();
                            },
                            error: function (data) {
                                var response = $.parseJSON(data.responseText);

                                if (!$.isEmptyObject(response.errors)) {
                                    $.each(response.errors, function (f, m) {
                                        scope.contactForm.find('div[data-field="' + f + '"]').removeClass('hidden').text(m);
                                    });
                                }
                            },
                            dataType: 'json'
                        });
                    });
                });
            },

            // Spinner toggle.
            spinnerToggle: function (flag) {
                var scope = this;

                if (flag === true) {
                    scope.spinner.spin();
                    scope.contactForm.after(scope.spinner.el);
                } else {
                    scope.spinner.stop();
                }
            },

            // Select 2.
            select2: function () {
                // Init select 2 lib.
                var select2ElementsMap = [
                    {
                        id: '#appbundle_hospital_country',
                        options: {
                            placeholder: Translator.trans('Country', {}, 'js'),
                            width: '100%',
                            allowClear: true
                        }
                    },
                    {
                        id: '#appbundle_user_roles',
                        options: {
                            placeholder: Translator.trans('Roles', {}, 'js'),
                            width: '100%'
                        }
                    },
                    {
                        id: '#appbundle_department_hospital',
                        options: {
                            placeholder: Translator.trans('Hospital', {}, 'js'),
                            width: '100%',
                            allowClear: true
                        }
                    },
                    {
                        id: '#appbundle_user_hospital',
                        options: {
                            placeholder: Translator.trans('Hospital', {}, 'js'),
                            width: '100%',
                            allowClear: true
                        }
                    },
                    {
                        id: '#fos_user_profile_form_department',
                        options: {
                            width: '100%'
                        }
                    },
                    {
                        id: '#appbundle_patient_filters_sex',
                        options: {
                            width: '100%',
                            allowClear: true,
                            placeholder: Translator.trans('patients_filters.placeholders.sex', {}, 'js')
                        }
                    },
                    {
                        id: '#appbundle_patient_filters_fullTerm',
                        options: {
                            width: '100%',
                            allowClear: true,
                            placeholder: Translator.trans('patients_filters.placeholders.is_full_term', {}, 'js')
                        }
                    },
                    {
                        id: '#appbundle_patient_filters_gestationalAge',
                        options: {
                            width: '100%',
                            allowClear: true,
                            placeholder: Translator.trans('patients_filters.placeholders.gestational_age', {}, 'js')
                        }
                    },
                    {
                        id: '#appbundle_patient_filters_pastIllnesses',
                        options: {
                            width: '100%',
                            placeholder: Translator.trans('patients_filters.placeholders.past_illnesses', {}, 'js'),
                            allowClear: true,
                            tags: true
                        }
                    },
                    {
                        id: '#appbundle_patient_filters_atopy',
                        options: {
                            width: '100%',
                            placeholder: Translator.trans('patients_filters.placeholders.atopy', {}, 'js'),
                            allowClear: true,
                            tags: true
                        }
                    },
                    {
                        id: '#appbundle_patient_filters_allergy',
                        options: {
                            width: '100%',
                            placeholder: Translator.trans('patients_filters.placeholders.allergy', {}, 'js'),
                            allowClear: true,
                            tags: true
                        }
                    },
                    {
                        id: '#appbundle_patient_filters_sensibilization',
                        options: {
                            width: '100%',
                            placeholder: Translator.trans('patients_filters.placeholders.sensibilization', {}, 'js'),
                            allowClear: true,
                            tags: true
                        }
                    },
                    {
                        id: '#appbundle_patient_filters_treatment',
                        options: {
                            width: '100%',
                            placeholder: Translator.trans('patients_filters.placeholders.treatment', {}, 'js'),
                            allowClear: true,
                            tags: true
                        }
                    },
                    {
                        id: '#appbundle_patient_filters_disease',
                        options: {
                            width: '100%',
                            allowClear: true,
                            placeholder: Translator.trans('patients_filters.placeholders.disease', {}, 'js'),
                            tags: true
                        }
                    },
                    {
                        id: '#appbundle_patient_filters_diseaseSeverity',
                        options: {
                            placeholder: Translator.trans('patients_filters.placeholders.disease_severity', {}, 'js'),
                            allowClear: true,
                            width: '100%'
                        }
                    },
                    {
                        id: '#appbundle_patient_filters_heredity',
                        options: {
                            width: '100%',
                            placeholder: Translator.trans('patients_filters.placeholders.heredity', {}, 'js'),
                            allowClear: true,
                            tags: true
                        }
                    },
                    {
                        id: '#appbundle_patient_filters_examination',
                        options: {
                            width: '100%',
                            placeholder: Translator.trans('patients_filters.placeholders.examination', {}, 'js'),
                            allowClear: true,
                            tags: true
                        }
                    },
                    {
                        id: '#appbundle_patient_filters_accompanyingIllnesses',
                        options: {
                            width: '100%',
                            placeholder: Translator.trans('patients_filters.placeholders.accompanying_illnesses', {}, 'js'),
                            allowClear: true,
                            tags: true
                        }
                    }
                ];

                $.each(select2ElementsMap, function (key, v) {
                    // Set language.
                    // todo: use language depending on current user language.
                    v.options.language = 'ru';

                    $(v.id).select2(v.options);
                });
            },

            // Navigation.
            navigation: function () {
                var sidebarMenu = $('ul.sidebar-menu');

                // Add active class to the parent menu item if sub menu item is checked.
                var currentPathLink = $('a[href="' + window.location.pathname + '"]', sidebarMenu);

                if (currentPathLink.length) {
                    currentPathLink.closest('.treeview').addClass('active');
                }
            },

            // Wysiwyg.
            wysiwyg: function () {
                // Init bootstrap wysiwyg.
                $('textarea').not('.contact-us').wysihtml5();
            },

            // Data tables.
            dataTables: function () {
                // Init Data tables.
                var dataTablesLngCDN = '//cdn.datatables.net/plug-ins/1.10.10/i18n/Russian.json',
                    dataTablesMap = [
                        {
                            id: '#attendances-dashboard',
                            options: {
                                stateSave: true,
                                columnDefs: [{
                                    targets: [1, 5],
                                    orderable: false
                                }],
                                order: [[2, 'desc']]
                            }
                        },
                        {
                            id: '#departments-dashboard',
                            options: {
                                stateSave: true,
                                columnDefs: [{
                                    targets: [4],
                                    orderable: false
                                }],
                                order: [[2, 'desc']]
                            }
                        },
                        {
                            id: '#hospitals-dashboard',
                            options: {
                                stateSave: true,
                                columnDefs: [{
                                    targets: [7],
                                    orderable: false
                                }],
                                order: [[5, 'desc']]
                            }
                        },
                        {
                            id: '#patients-dashboard',
                            options: {
                                stateSave: true,
                                columnDefs: [{
                                    targets: [4, 5, 6, 9],
                                    orderable: false
                                }],
                                order: [[7, 'desc']]
                            }
                        },
                        {
                            id: '#users-dashboard',
                            options: {
                                stateSave: true,
                                columnDefs: [{
                                    targets: [4, 5, 6, 8, 9, 10],
                                    orderable: false
                                }],
                                order: [[7, 'desc']]
                            }
                        },
                        {
                            id: '#patient-attendances-dashboard',
                            options: {
                                stateSave: true,
                                columnDefs: [{
                                    targets: [1, 4],
                                    orderable: false
                                }],
                                order: [[0, 'desc']]
                            }
                        },
                        {
                            id: '#my-patients-dashboard',
                            options: {
                                stateSave: true,
                                columnDefs: [{
                                    targets: [4, 5, 6, 7],
                                    orderable: false
                                }],
                                order: [[5, 'desc']]
                            }
                        },
                        {
                            id: '#all-attendances-dashboard',
                            options: {
                                stateSave: true,
                                columnDefs: [{
                                    targets: [1, 2, 5],
                                    orderable: false
                                }],
                                order: [[0, 'desc']]
                            }
                        },
                        {
                            id: '#patients-filters',
                            options: {
                                stateSave: true,
                                columnDefs: [{
                                    targets: [3, 4, 5],
                                    orderable: false
                                }],
                            }
                        }
                    ];

                $.each(dataTablesMap, function (i, v) {
                    // Set language.
                    v.options.language = {
                        // todo: use language cdn link depending on current user language.
                        'url': dataTablesLngCDN
                    };

                    $(v.id).DataTable(v.options);
                });
            },

            // iCheck.
            iCheck: function () {
                $('input[type="checkbox"]').iCheck({
                    checkboxClass: 'icheckbox_flat-red'
                });
            }
        };
    };

    // App init.
    $(document).ready(function () {
        window.App = new App({
            contactForm: '#contact-us-form',
            contactFormBtn: '#contact-us-modal button',
            contactFormModal: '#contact-us-modal'
        });
        window.App.init();
    });
})(jQuery);