/**
 * @file Attendances dashboard js.
 */
(function ($) {
    /**
     * Attendances
     *
     * @returns {{init: Function, niceScroll: Function}}
     * @constructor
     */
    var Attendances = function () {
        return {
            init: function () {
                var scope = this;

                // Nice scroll.
                scope.niceScroll();
            },

            // Nice scroll.
            niceScroll: function () {
                // Init nice scroll for attendances past/today/future visits block.
                $('.timed-attendances-sidebar').niceScroll();
            }
        };
    };

    // Dashboard init.
    $(document).ready(function () {
        window.Attendances = new Attendances();
        window.Attendances.init();
    });
})(jQuery);