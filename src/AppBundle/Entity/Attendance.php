<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Patient;
use Doctrine\ORM\Mapping as ORM;

/**
 * Attendance
 *
 * @ORM\Table(name="attendance")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\AttendanceRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Attendance
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date", nullable=false)
     */
    protected $date;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    protected $description;

    /**
     * @var string
     *
     * @ORM\Column(name="therapy", type="text", nullable=true)
     */
    protected $therapy;

    /**
     * @var string
     *
     * @ORM\Column(name="reason", type="string", length=255, nullable=false)
     */
    protected $reason;

    /**
     * @var string
     *
     * @ORM\Column(name="isAttended", type="boolean", nullable=true)
     */
    protected $isAttended;

    /**
     * @var string
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    protected $createdAt;

    /**
     * @var string
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    protected $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity="Patient", inversedBy="attendances")
     * @ORM\JoinColumn(name="patient_id", referencedColumnName="id")
     */
    protected $patient;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Attendance
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Attendance
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set therapy
     *
     * @param string $therapy
     * @return Attendance
     */
    public function setTherapy($therapy)
    {
        $this->therapy = $therapy;

        return $this;
    }

    /**
     * Get therapy
     *
     * @return string
     */
    public function getTherapy()
    {
        return $this->therapy;
    }

    /**
     * Set reason
     *
     * @param string $reason
     * @return Attendance
     */
    public function setReason($reason)
    {
        $this->reason = $reason;

        return $this;
    }

    /**
     * Get reason
     *
     * @return string
     */
    public function getReason()
    {
        return $this->reason;
    }

    /**
     * Set patient
     *
     * @param Patient $patient
     * @return Attendance
     */
    public function setPatient(Patient $patient = null)
    {
        $this->patient = $patient;

        return $this;
    }

    /**
     * Get patient
     *
     * @return Patient
     */
    public function getPatient()
    {
        return $this->patient;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Attendance
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Attendance
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Is the given User the doctor of this attendance.
     *
     * @param \AppBundle\Entity\User|null $user
     *
     * @return bool
     */
    public function isDoctor(User $user = null)
    {
        $userPatientsCollection = $user->getPatients();

        $userPatientsIdArr = $userPatientsCollection->map(
            function ($entity) {
                return $entity->getId();
            }
        )->toArray();

        return $user && in_array($this->patient->getId(), $userPatientsIdArr);
    }

    /**
     * Set createdAt and updatedAt values during department creation.
     *
     * @ORM\PrePersist
     */
    public function setCreatedAtAndUpdatedAtValues()
    {
        $this->createdAt = $this->updatedAt = new \DateTime();
    }

    /**
     * Set updatedAt value during department update.
     *
     * @ORM\PreUpdate()
     */
    public function setUpdatedAtValue()
    {
        $this->updatedAt = new \DateTime();
    }

    /**
     * Set isAttended
     *
     * @param boolean $isAttended
     * @return Attendance
     */
    public function setIsAttended($isAttended)
    {
        $this->isAttended = $isAttended;

        return $this;
    }

    /**
     * Get isAttended
     *
     * @return boolean
     */
    public function getIsAttended()
    {
        return $this->isAttended;
    }
}
