<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Attendance;
use AppBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Patient
 *
 * @ORM\Table(name="patient")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\PatientRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Patient
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="firstName", type="string", length=255, nullable=false)
     */
    protected $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="lastName", type="string", length=255, nullable=false)
     */
    protected $lastName;

    /**
     * @var string
     *
     * @ORM\Column(name="middleName", type="string", length=255, nullable=true)
     */
    protected $middleName;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="birthday", type="date", nullable=false)
     */
    protected $birthday;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    protected $email;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=255, nullable=true)
     */
    protected $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255, nullable=true)
     */
    protected $address;

    /**
     * @var
     *
     * @ORM\Column(name="sex", type="string", length=255, nullable=true)
     */
    protected $sex;

    /**
     * @var
     *
     * @ORM\Column(name="fullTerm", type="boolean", nullable=true)
     */
    protected $fullTerm;

    /**
     * @var
     *
     * @ORM\Column(name="gestationalAge", type="integer", nullable=true)
     */
    protected $gestationalAge;

    /**
     * @var
     *
     * @ORM\Column(name="pastIllnesses", type="array", nullable=true)
     */
    protected $pastIllnesses;


    /**
     * @var
     *
     * @ORM\Column(name="pastIllnessesDate", type="json_array", nullable=true)
     */
    protected $pastIllnessesDate;

    /**
     * @var
     *
     * @ORM\Column(name="atopy", type="string", length=255, nullable=true)
     */
    protected $atopy;

    /**
     * @var
     *
     * @ORM\Column(name="allergy", type="array", nullable=true)
     */
    protected $allergy;

    /**
     * @var
     *
     * @ORM\Column(name="allergyDate", type="json_array", nullable=true)
     */
    protected $allergyDate;

    /**
     * @var
     *
     * @ORM\Column(name="sensibilization", type="array", nullable=true)
     */
    protected $sensibilization;

    /**
     * @var
     *
     * @ORM\Column(name="treatment", type="array", nullable=true)
     */
    protected $treatment;

    /**
     * @var
     *
     * @ORM\Column(name="commonData", type="text", nullable=true)
     */
    protected $commonData;

    /**
     * @var
     *
     * @ORM\Column(name="disease", type="array", nullable=true)
     */
    protected $disease;

    /**
     * @var
     *
     * @ORM\Column(name="diseaseDate", type="json_array", nullable=true)
     */
    protected $diseaseDate;

    /**
     * @var
     *
     * @ORM\Column(name="diseaseSeverity", type="smallint", nullable=true)
     */
    protected $diseaseSeverity;

    /**
     * @var
     *
     * @ORM\Column(name="heredity", type="array", nullable=true)
     */
    protected $heredity;

    /**
     * @var
     *
     * @ORM\Column(name="heredityDisease", type="json_array", nullable=true)
     */
    protected $heredityDisease;

    /**
     * @var
     *
     * @ORM\Column(name="examination", type="array", nullable=true)
     */
    protected $examination;

    /**
     * @var
     *
     * @ORM\Column(name="examinationDate", type="json_array", nullable=true)
     */
    protected $examinationDate;

    /**
     * @var
     *
     * @ORM\Column(name="accompanyingIllnesses", type="array", nullable=true)
     */
    protected $accompanyingIllnesses;

    /**
     * @var
     */
    protected $filters;

    /**
     * @var string
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    protected $createdAt;

    /**
     * @var string
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    protected $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="patients")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    protected $user;

    /**
     * @ORM\OneToMany(targetEntity="Attendance", mappedBy="patient", cascade={"remove"})
     */
    protected $attendances;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->attendances = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     * @return Patient
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     * @return Patient
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set middleName
     *
     * @param string $middleName
     * @return Patient
     */
    public function setMiddleName($middleName)
    {
        $this->middleName = $middleName;

        return $this;
    }

    /**
     * Get middleName
     *
     * @return string
     */
    public function getMiddleName()
    {
        return $this->middleName;
    }

    /**
     * Set birthday
     *
     * @param \DateTime $birthday
     * @return Patient
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;

        return $this;
    }

    /**
     * Get birthday
     *
     * @return \DateTime
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Patient
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return Patient
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Patient
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Patient
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set user
     *
     * @param User $user
     * @return Patient
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Is the given User the doctor of this patient.
     *
     * @param \AppBundle\Entity\User|null $user
     *
     * @return bool
     */
    public function isDoctor(User $user = null)
    {
        return $user && $user->getId() === $this->user->getId();
    }

    /**
     * Add attendances
     *
     * @param Attendance $attendances
     * @return Patient
     */
    public function addAttendance(Attendance $attendances)
    {
        $this->attendances[] = $attendances;

        return $this;
    }

    /**
     * Remove attendances
     *
     * @param Attendance $attendances
     */
    public function removeAttendance(Attendance $attendances)
    {
        $this->attendances->removeElement($attendances);
    }

    /**
     * Get attendances
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAttendances()
    {
        return $this->attendances;
    }

    /**
     * Set createdAt and updatedAt values during patient creation.
     *
     * @ORM\PrePersist
     */
    public function setCreatedAtAndUpdatedAtValues()
    {
        $this->createdAt = $this->updatedAt = new \DateTime();
    }

    /**
     * Set updatedAt value during patient update.
     *
     * @ORM\PreUpdate()
     */
    public function setUpdatedAtValue()
    {
        $this->updatedAt = new \DateTime();
    }

    /**
     * Set address
     *
     * @param string $address
     * @return Patient
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set sex
     *
     * @param string $sex
     * @return Patient
     */
    public function setSex($sex)
    {
        $this->sex = $sex;

        return $this;
    }

    /**
     * Get sex
     *
     * @return string
     */
    public function getSex()
    {
        return $this->sex;
    }

    /**
     * Set fullTerm
     *
     * @param boolean $fullTerm
     * @return Patient
     */
    public function setFullTerm($fullTerm)
    {
        $this->fullTerm = $fullTerm;

        return $this;
    }

    /**
     * Get fullTerm
     *
     * @return boolean
     */
    public function getFullTerm()
    {
        return $this->fullTerm;
    }

    /**
     * Set gestationalAge
     *
     * @param integer $gestationalAge
     * @return Patient
     */
    public function setGestationalAge($gestationalAge)
    {
        $this->gestationalAge = $gestationalAge;

        return $this;
    }

    /**
     * Get gestationalAge
     *
     * @return integer
     */
    public function getGestationalAge()
    {
        return $this->gestationalAge;
    }

    /**
     * Set atopy
     *
     * @param string $atopy
     * @return Patient
     */
    public function setAtopy($atopy)
    {
        $this->atopy = $atopy;

        return $this;
    }

    /**
     * Get atopy
     *
     * @return string
     */
    public function getAtopy()
    {
        return $this->atopy;
    }

    /**
     * Set pastIllnesses
     *
     * @param array $pastIllnesses
     * @return Patient
     */
    public function setPastIllnesses($pastIllnesses)
    {
        $this->pastIllnesses = $pastIllnesses;

        return $this;
    }

    /**
     * Get pastIllnesses
     *
     * @return array
     */
    public function getPastIllnesses()
    {
        return $this->pastIllnesses;
    }

    /**
     * Set allergy
     *
     * @param array $allergy
     * @return Patient
     */
    public function setAllergy($allergy)
    {
        $this->allergy = $allergy;

        return $this;
    }

    /**
     * Get allergy
     *
     * @return array
     */
    public function getAllergy()
    {
        return $this->allergy;
    }

    /**
     * Set sensibilization
     *
     * @param array $sensibilization
     * @return Patient
     */
    public function setSensibilization($sensibilization)
    {
        $this->sensibilization = $sensibilization;

        return $this;
    }

    /**
     * Get sensibilization
     *
     * @return array
     */
    public function getSensibilization()
    {
        return $this->sensibilization;
    }

    /**
     * Set treatment
     *
     * @param array $treatment
     * @return Patient
     */
    public function setTreatment($treatment)
    {
        $this->treatment = $treatment;

        return $this;
    }

    /**
     * Get treatment
     *
     * @return array
     */
    public function getTreatment()
    {
        return $this->treatment;
    }

    /**
     * Set commonData
     *
     * @param string $commonData
     * @return Patient
     */
    public function setCommonData($commonData)
    {
        $this->commonData = $commonData;

        return $this;
    }

    /**
     * Get commonData
     *
     * @return string
     */
    public function getCommonData()
    {
        return $this->commonData;
    }

    /**
     * Set disease
     *
     * @param string $disease
     * @return Patient
     */
    public function setDisease($disease)
    {
        $this->disease = $disease;

        return $this;
    }

    /**
     * Get disease
     *
     * @return string
     */
    public function getDisease()
    {
        return $this->disease;
    }

    /**
     * Set diseaseSeverity
     *
     * @param integer $diseaseSeverity
     * @return Patient
     */
    public function setDiseaseSeverity($diseaseSeverity)
    {
        $this->diseaseSeverity = $diseaseSeverity;

        return $this;
    }

    /**
     * Get diseaseSeverity
     *
     * @return integer
     */
    public function getDiseaseSeverity()
    {
        return $this->diseaseSeverity;
    }

    /**
     * Set heredity
     *
     * @param array $heredity
     * @return Patient
     */
    public function setHeredity($heredity)
    {
        $this->heredity = $heredity;

        return $this;
    }

    /**
     * Get heredity
     *
     * @return array
     */
    public function getHeredity()
    {
        return $this->heredity;
    }

    /**
     * Set examination
     *
     * @param array $examination
     * @return Patient
     */
    public function setExamination($examination)
    {
        $this->examination = $examination;

        return $this;
    }

    /**
     * Get examination
     *
     * @return array
     */
    public function getExamination()
    {
        return $this->examination;
    }

    /**
     * Set accompanyingIllnesses
     *
     * @param array $accompanyingIllnesses
     * @return Patient
     */
    public function setAccompanyingIllnesses($accompanyingIllnesses)
    {
        $this->accompanyingIllnesses = $accompanyingIllnesses;

        return $this;
    }

    /**
     * Get accompanyingIllnesses
     *
     * @return array
     */
    public function getAccompanyingIllnesses()
    {
        return $this->accompanyingIllnesses;
    }

    /**
     * Get filters.
     *
     * @return mixed
     */
    public function getFilters()
    {
        return $this->filters;
    }

    /**
     * Set filters.
     *
     * @param mixed $filters
     * @return mixed
     */
    public function setFilters($filters)
    {
        $filtersFields = [
            'sex',
            'fullTerm',
            'gestationalAge',
            'pastIllnesses',
            'atopy',
        ];

        // Set values for appropriate fields.
        foreach ($filtersFields as $field) {
            if (property_exists($filters, $field)) {
                $this->{'set'.ucfirst($field)}($filters->$field);
            }
        }

        $this->filters = $filters;

        return $this;
    }

    /**
     * Set diseaseDate
     *
     * @param array $diseaseDate
     * @return Patient
     */
    public function setDiseaseDate($diseaseDate)
    {
        $this->diseaseDate = $diseaseDate;

        return $this;
    }

    /**
     * Get diseaseDate
     *
     * @return array
     */
    public function getDiseaseDate()
    {
        return $this->diseaseDate;
    }

    /**
     * Set heredityDisease
     *
     * @param array $heredityDisease
     * @return Patient
     */
    public function setHeredityDisease($heredityDisease)
    {
        $this->heredityDisease = $heredityDisease;

        return $this;
    }

    /**
     * Get heredityDisease
     *
     * @return array
     */
    public function getHeredityDisease()
    {
        return $this->heredityDisease;
    }

    /**
     * Set examinationDate
     *
     * @param array $examinationDate
     * @return Patient
     */
    public function setExaminationDate($examinationDate)
    {
        $this->examinationDate = $examinationDate;

        return $this;
    }

    /**
     * Get examinationDate
     *
     * @return array
     */
    public function getExaminationDate()
    {
        return $this->examinationDate;
    }

    /**
     * Set pastIllnessesDate
     *
     * @param array $pastIllnessesDate
     * @return Patient
     */
    public function setPastIllnessesDate($pastIllnessesDate)
    {
        $this->pastIllnessesDate = $pastIllnessesDate;

        return $this;
    }

    /**
     * Get pastIllnessesDate
     *
     * @return array
     */
    public function getPastIllnessesDate()
    {
        return $this->pastIllnessesDate;
    }

    /**
     * Set allergyDate
     *
     * @param array $allergyDate
     * @return Patient
     */
    public function setAllergyDate($allergyDate)
    {
        $this->allergyDate = $allergyDate;

        return $this;
    }

    /**
     * Get allergyDate
     *
     * @return array
     */
    public function getAllergyDate()
    {
        return $this->allergyDate;
    }
}
