<?php
namespace AppBundle\EventListener;

// ...

use AppBundle\Model\MenuItemModel;
use Avanzu\AdminThemeBundle\Event\SidebarMenuEvent;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class MenuItemListListener
 * @package AppBundle\EventListener
 */
class MenuItemListListener
{

    /**
     * @var ContainerInterface
     */
    private $container;


    /**
     * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;

        $this->translator = $container->get('translator');
    }

    /**
     * @param \Avanzu\AdminThemeBundle\Event\SidebarMenuEvent $event
     */
    public function onSetupMenu(SidebarMenuEvent $event)
    {

        $request = $event->getRequest();

        foreach ($this->getMenu($request) as $item) {
            $event->addItem($item);
        }

    }

    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return mixed
     */
    protected function getMenu(Request $request)
    {
        // Authenticated user.
        $securityContext = $this->container->get('security.context');

        // Build your menu here by constructing a MenuItemModel array.
        $menuItems = [];

        switch (true) {
            // Menu items for admin.
            case $securityContext->isGranted('ROLE_ADMIN'):
                $menuItems = [
                    new MenuItemModel(
                        'admin',
                        $this->translator->trans('Admin entities'),
                        null,
                        [],
                        [
                            // Users.
                            new MenuItemModel(
                                'admin_users',
                                $this->translator->trans('Users'),
                                'admin_user_entity_list',
                                [],
                                [],
                                'fa fa-fw fa-users'
                            ),
                            // Hospitals.
                            new MenuItemModel(
                                'admin_hospitals',
                                $this->translator->trans('Hospitals'),
                                'admin_hospital_entity_list',
                                [],
                                [],
                                'fa fa-fw fa-hospital-o'
                            ),
                            // Departments.
                            new MenuItemModel(
                                'admin_departments',
                                $this->translator->trans('Departments'),
                                'admin_department_entity_list',
                                [],
                                [],
                                'fa fa-fw fa-sitemap'
                            ),
                            // Patients.
                            new MenuItemModel(
                                'admin_patients',
                                $this->translator->trans('Patients'),
                                'admin_patient_entity_list',
                                [],
                                [],
                                'fa fa-fw fa-user'
                            ),
                            // Patients.
                            new MenuItemModel(
                                'admin_attendances',
                                $this->translator->trans('Attendances'),
                                'admin_attendance_entity_list',
                                [],
                                [],
                                'fa fa-fw fa-stethoscope'
                            ),
                        ],
                        'fa fa-cogs'
                    ),
                ];
                break;

            // Menu items for doctor.
            case $securityContext->isGranted('ROLE_DOCTOR'):
                $menuItems = [
                    // My patients.
                    new MenuItemModel(
                        'dashboard',
                        $this->translator->trans('My patients'),
                        'dashboard',
                        [],
                        [],
                        'fa fa-dashboard'
                    ),
                    // Attendances.
                    new MenuItemModel(
                        'attendances',
                        $this->translator->trans('Attendances'),
                        'attendances_list',
                        [],
                        [],
                        'fa fa-fw fa-stethoscope'
                    ),
                    // Patients filter.
                    new MenuItemModel(
                        'patients_filters',
                        $this->translator->trans('menu_item.patients_filters'),
                        'patients_filters',
                        [],
                        [],
                        'fa fa-fw fa-filter'
                    ),
                ];
                break;
        }

        return $this->activateByRoute($request->get('_route'), $menuItems);
    }

    /**
     * @param $route
     * @param $items
     * @return mixed
     */
    protected function activateByRoute($route, $items)
    {

        foreach ($items as $item) {
            if ($item->hasChildren()) {
                $this->activateByRoute($route, $item->getChildren());
            } else {
                if ($item->getRoute() == $route) {
                    $item->setIsActive(true);
                }
            }
        }

        return $items;
    }

}