<?php

namespace AppBundle\EventListener;

use Avanzu\AdminThemeBundle\Event\ShowUserEvent;
use AppBundle\Model\UserModel;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ShowUserListener
{

    private $container;

    /**
     * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param \Avanzu\AdminThemeBundle\Event\ShowUserEvent $event
     */
    public function onShowUser(ShowUserEvent $event)
    {

        $user = $this->getUser();
        $event->setUser($user);

    }

    /**
     * @return mixed
     */
    protected function getUser()
    {
        // Authenticated user.
        $user = $this->container->get('security.context')->getToken()->getUser();

        return new UserModel(
            $user->getFullName(),
            $user->getImage(),
            $user->getMemberSince()->format('Y-m-d'),
            true,
            $user->getFullName(),
            $user->getFullName()
        );
    }

}