<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Hospital;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class LoadHospitalData
 * @package AppBundle\DataFixtures\ORM
 */
class LoadHospitalData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $hospitals = [
            1 => [
                'name' => 'Fixture hospital',
                'region' => 'ZP',
                'city' => 'ZP',
                'country' => 'UA',
                'address' => 'Some street'
            ]
        ];

        foreach ($hospitals as $key => $hospitalInfo) {
            $hospital = new Hospital();

            foreach ($hospitalInfo as $k => $v) {
                $hospital->{'set' . ucfirst($k)}($v);
            }

            $manager->persist($hospital);

            $this->setReference('hospital_' . $key, $hospital);
        }

        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 1;
    }
}
