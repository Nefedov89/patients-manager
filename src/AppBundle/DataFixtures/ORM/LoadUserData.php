<?php

namespace AppBundle\DataFixtures\ORM;

use DateTime;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Application\Sonata\MediaBundle\Entity\Media;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use AppBundle\Entity\User;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;

/**
 * Class LoadUserData
 * @package AppBundle\DataFixtures\ORM
 */
class LoadUserData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $users = [
            [
                'username' => 'doctor',
                'email' => 'john_doe@m.com',
                'pass' => 'pass',
                'roles' => ['ROLE_DOCTOR'],
                'first_name' => 'John',
                'last_name' => 'Doe',
                'skype' => 'john_doe',
                'position' => 'doctor',
                'member_since' => new DateTime('now'),
                'education' => 'ZP Medical university',
                'phone' => '3(8)099-111-11-11',
            ],
            [
                'username' => 'admin',
                'email' => 'nefedovs89@gmail.com',
                'pass' => 'pass',
                'roles' => ['ROLE_ADMIN'],
                'first_name' => 'Sergey',
                'last_name' => 'Nefedov',
                'skype' => 'seriy_nefedov',
                'position' => 'admin',
                'member_since' => new DateTime('now'),
                'education' => 'ZSEA',
                'phone' => '0993591557',
            ],
        ];

        foreach ($users as $key => $userInfo) {
            $user = new User();

            foreach ($userInfo as $k => $v) {
                if ($k == 'pass') {
                    continue;
                }
                $user->{'set'.str_replace(' ', '', ucwords(str_replace('_', '', $k)))}($v);
            }

            $user->setEnabled(true);

            // Set hospital.
            $user->setHospital($this->getReference('hospital_1'));

            // Set user picture.
            $this->attachImages($manager, $user, $key, 'picture', 'user_pic_', 'setImage');

            // Set password.
            $encoder = $this->container
                ->get('security.encoder_factory')
                ->getEncoder($user);
            $user->setPassword($encoder->encodePassword($userInfo['pass'], $user->getSalt()));

            $manager->persist($user);
        }

        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 2;
    }

    /**
     * Method that attach user picture when fixture generate User entity.
     *
     * @param ObjectManager $manager
     * @param object $user
     * @param string $imgFolder Images folder name
     * @param string $imgNamePattern Images name pattern ('user_pic_', 'user_signature_' ... )
     * @param string $setMethodName Setter name (setImage, setSignature)
     *
     * @param integer $key users index during iteration.
     */
    private function attachImages($manager, $user, $key, $imgFolder, $imgNamePattern, $setMethodName)
    {
        // Name of media provider.
        $provider = 'sonata.media.provider.image';
        // Media context.
        $context = 'default';

        /**
         * @var Finder $finder
         *
         * Find a file to save in media.
         */
        $finder = new Finder();
        $finder->in('app/Resources/DataFixtures/img/user/'.$imgFolder);
        $finder->name($imgNamePattern.$key.'.png');

        /**
         * @var SplFileInfo $splFileInfo
         */
        foreach ($finder as $splFileInfo) {
            // Variable to store path to file (image).
            $path = $splFileInfo->getRealPath();
            /**
             * @var Media $media
             */
            $media = new Media();
            $media->setBinaryContent($path);
            $media->setEnabled(true);
            $media->setProviderName($provider);
            $media->setContext($context);

            $manager->persist($media);
            $manager->flush();

            $user->$setMethodName($media);
        }
    }
}
