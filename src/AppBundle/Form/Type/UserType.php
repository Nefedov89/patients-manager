<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class UserType
 * @package AppBundle\Form\Type
 */
class UserType extends AbstractType
{
    /**
     * Method for building user creation form.
     *
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username')
            ->add('email')
            ->add('firstName')
            ->add('lastName')
            ->add('skype')
            ->add('position')
            ->add('education')
            ->add('phone')
            ->add(
                'roles',
                'choice',
                [
                    'choices' => [
                        'ROLE_DOCTOR' => 'Doctor',
                        'ROLE_MODERATOR' => 'Moderator',
                    ],
                    'multiple' => true,
                ]
            )
            ->add(
                'hospital',
                'entity',
                [
                    'class' => 'AppBundle:Hospital',
                    'property' => 'name',
                ]
            )
            ->add('plainPassword', 'password')
            ->add('submit', 'submit');
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => 'AppBundle\Entity\User',
                'validation_groups' => ['edit', 'create', 'Profile'],
            ]
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_user';
    }
}
