<?php

namespace AppBundle\Form\Type;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class ProfileType
 * @package AppBundle\Form\Type
 */
class ProfileType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'image',
                'app_bundle_image_upload',
                [
                    'required' => false,
                ]
            )
            ->add(
                'firstName',
                null,
                [
                    'label' => 'First name',
                    'translation_domain' => 'profile',
                ]
            )
            ->add(
                'lastName',
                null,
                [
                    'label' => 'Last name',
                    'translation_domain' => 'profile',
                ]
            )
            ->add(
                'position',
                null,
                [
                    'label' => 'Position',
                    'translation_domain' => 'profile',
                ]
            )
            ->add(
                'department',
                'entity',
                [
                    'class' => 'AppBundle:Department',
                    'property' => 'name',
                    'query_builder' => function (EntityRepository $er) use ($options) {
                        return $er->createQueryBuilder('d')
                            ->where('d.hospital = :hospital')
                            ->setParameter('hospital', $options['data']->getHospital()->getId())
                            ->orderBy('d.name', 'ASC');
                    },
                    'required' => false,
                    'label' => 'Department',
                    'translation_domain' => 'profile',
                ]
            )
            ->add(
                'skype',
                null,
                [
                    'label' => 'Skype',
                    'translation_domain' => 'profile',
                ]
            )
            ->add('email')
            ->add(
                'education',
                null,
                [
                    'label' => 'Education',
                    'translation_domain' => 'profile',
                ]
            )
            ->add(
                'phone',
                null,
                [
                    'label' => 'Phone',
                    'translation_domain' => 'profile',
                ]
            )
            ->add(
                'plain_password',
                'repeated',
                [
                    'type' => 'password',
                    'error_bubbling' => false,
                    'invalid_message' => 'The password fields must match.',
                    'options' => ['attr' => ['class' => 'password-field']],
                    'required' => false,
                    'first_options' => [
                        'label' => '',
                        'attr' => [
                            'placeholder' => 'Password',
                        ],
                    ],
                    'second_options' => [
                        'label' => '',
                        'attr' => [
                            'placeholder' => 'Repeat Password',
                        ],
                    ],
                ]
            );
    }

    /**
     * @return string
     */
    public function getParent()
    {
        return 'fos_user_profile';
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'app_bundle_user_profile';
    }
}

