<?php

namespace AppBundle\Form\Type;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class PatientFiltersType
 * @package AppBundle\Form\Type
 */
class PatientFiltersType extends AbstractType
{
    /**
     * @var array
     */
    protected $patientFiltersFieldsMap = [
        'sex' => [
            'customTag' => false,
            'type' => 'string',
            'options' => null,
        ],
        'fullTerm' => [
            'customTag' => false,
            'type' => 'string',
            'options' => null,
        ],
        'gestationalAge' => [
            'customTag' => false,
            'type' => 'string',
            'options' => null,
        ],
        'pastIllnesses' => [
            'customTag' => true,
            'type' => 'array',
            'options' => [],
        ],
        'atopy' => [
            'customTag' => true,
            'type' => 'string',
            'options' => [],
        ],
        'allergy' => [
            'customTag' => true,
            'type' => 'array',
            'options' => [],
        ],
        'sensibilization' => [
            'customTag' => true,
            'type' => 'array',
            'options' => [],
        ],
        'treatment' => [
            'customTag' => true,
            'type' => 'array',
            'options' => [],
        ],
        'disease' => [
            'customTag' => true,
            'type' => 'array',
            'options' => [],
        ],
        'diseaseSeverity' => [
            'customTag' => false,
            'type' => 'string',
            'options' => null,
        ],
        'heredity' => [
            'customTag' => true,
            'type' => 'array',
            'options' => [],
        ],
        'examination' => [
            'customTag' => true,
            'type' => 'array',
            'options' => [],
        ],
        'accompanyingIllnesses' => [
            'customTag' => true,
            'type' => 'array',
            'options' => [],
        ],
    ];

    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * @param EntityManager       $em
     * @param TranslatorInterface $translator
     */
    public function __construct(EntityManager $em, TranslatorInterface $translator)
    {
        $this->em = $em;
        $this->translator = $translator;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // Build fields.
        foreach ($this->patientFiltersFieldsMap as $field => $info) {
            $this->buildChoiceField($builder, $field, $info['options']);
        }

        // Pre submit hook.
        // Set custom tags for choice fields during create/edit patient.
        $builder->addEventListener(
            FormEvents::PRE_SUBMIT,
            function (FormEvent $event) {
                $data = $event->getData();
                $form = $event->getForm();

                // Set additional tags dynamically.
                foreach ($this->patientFiltersFieldsMap as $field => $info) {
                    if (!$info['customTag']) {
                        continue;
                    }

                    $newFieldData = [];

                    if ($info['type'] == 'array' && isset($data[$field])) {
                        foreach ($data[$field] as $v) {
                            $newFieldData[$v] = $v;
                        }
                    } elseif ($info['type'] == 'string') {
                        $newFieldData = [$data[$field] => $data[$field]];
                    }

                    $this->buildChoiceField($form, $field, $newFieldData);
                }
            }
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_patient_filters';
    }

    /**
     * Build filter field.
     *
     * @param $builder |$form
     * @param $field
     * @param array $options
     */
    private function buildChoiceField(&$builder, $field, $options = [])
    {
        // Get all unique fields values from DB.
        $fieldValuesFromDb = [];

        // Complement default option values with previously submitted custom tags.
        if (empty($options) && !is_null($options)) {
            $entityRepository = $this->em->getRepository('AppBundle:Patient');
            $qb = $entityRepository
                ->createQueryBuilder('p')
                ->select('DISTINCT p.'.$field)
                ->where('p.'.$field.' IS NOT NULL');
            $result = $qb->getQuery()->getResult();

            if (!empty($result)) {
                foreach ($result as $item) {
                    $fieldVal = $item[$field];

                    if (!is_null($fieldVal) && !empty($fieldVal)) {
                        switch (true) {
                            case is_array($fieldVal):
                                foreach ($fieldVal as $v) {
                                    $fieldValuesFromDb[(string) $v] = $v;
                                }
                                break;
                            case is_string($fieldVal):
                                $fieldValuesFromDb[(string) $fieldVal] = $fieldVal;
                                break;
                        }
                    }
                }
            }
        }

        /**
         * Some default common options.
         */
        // Allergy categories.
        $allergyCategories = [
            $this->translator->trans('patient.fields.allergy.household_allergens', [], 'patient') => [
                $this->translator->trans(
                    'patient.fields.allergy.household_allergens.house_dust_1',
                    [],
                    'patient'
                ) => $this->translator->trans('patient.fields.allergy.household_allergens.house_dust_1', [], 'patient'),
                $this->translator->trans(
                    'patient.fields.allergy.household_allergens.house_dust_2',
                    [],
                    'patient'
                ) => $this->translator->trans('patient.fields.allergy.household_allergens.house_dust_2', [], 'patient'),
                $this->translator->trans(
                    'patient.fields.allergy.household_allergens.cat_hair',
                    [],
                    'patient'
                ) => $this->translator->trans('patient.fields.allergy.household_allergens.cat_hair', [], 'patient'),
                $this->translator->trans(
                    'patient.fields.allergy.household_allergens.dog_hair',
                    [],
                    'patient'
                ) => $this->translator->trans('patient.fields.allergy.household_allergens.dog_hair', [], 'patient'),
            ],
            $this->translator->trans('patient.fields.allergy.pollen_allergens', [], 'patient') => [
                $this->translator->trans(
                    'patient.fields.allergy.pollen_allergens.ambrosia',
                    [],
                    'patient'
                ) => $this->translator->trans('patient.fields.allergy.pollen_allergens.ambrosia', [], 'patient'),
                $this->translator->trans(
                    'patient.fields.allergy.pollen_allergens.sagebrush',
                    [],
                    'patient'
                ) => $this->translator->trans('patient.fields.allergy.pollen_allergens.sagebrush', [], 'patient'),
                $this->translator->trans(
                    'patient.fields.allergy.pollen_allergens.quinoa',
                    [],
                    'patient'
                ) => $this->translator->trans(
                    'patient.fields.allergy.pollen_allergens.quinoa',
                    [],
                    'patient'
                ),
                $this->translator->trans(
                    'patient.fields.allergy.pollen_allergens.sunflower',
                    [],
                    'patient'
                ) => $this->translator->trans('patient.fields.allergy.pollen_allergens.sunflower', [], 'patient'),
                $this->translator->trans(
                    'patient.fields.allergy.pollen_allergens.corn',
                    [],
                    'patient'
                ) => $this->translator->trans(
                    'patient.fields.allergy.pollen_allergens.corn',
                    [],
                    'patient'
                ),
                $this->translator->trans(
                    'patient.fields.allergy.pollen_allergens.cyclachaena',
                    [],
                    'patient'
                ) => $this->translator->trans('patient.fields.allergy.pollen_allergens.cyclachaena', [], 'patient'),
                $this->translator->trans(
                    'patient.fields.allergy.pollen_allergens.dandelion',
                    [],
                    'patient'
                ) => $this->translator->trans('patient.fields.allergy.pollen_allergens.dandelion', [], 'patient'),
                $this->translator->trans(
                    'patient.fields.allergy.pollen_allergens.birch',
                    [],
                    'patient'
                ) => $this->translator->trans(
                    'patient.fields.allergy.pollen_allergens.birch',
                    [],
                    'patient'
                ),
                $this->translator->trans(
                    'patient.fields.allergy.pollen_allergens.plantain',
                    [],
                    'patient'
                ) => $this->translator->trans('patient.fields.allergy.pollen_allergens.plantain', [], 'patient'),
                $this->translator->trans(
                    'patient.fields.allergy.pollen_allergens.nettle',
                    [],
                    'patient'
                ) => $this->translator->trans(
                    'patient.fields.allergy.pollen_allergens.nettle',
                    [],
                    'patient'
                ),
                $this->translator->trans(
                    'patient.fields.allergy.pollen_allergens.lolium_perenne',
                    [],
                    'patient'
                ) => $this->translator->trans('patient.fields.allergy.pollen_allergens.lolium_perenne', [], 'patient'),
            ],
            $this->translator->trans('patient.fields.allergy.food_allergens', [], 'patient') => [
                $this->translator->trans(
                    'patient.fields.allergy.food_allergens.cows_milk',
                    [],
                    'patient'
                ) => $this->translator->trans(
                    'patient.fields.allergy.food_allergens.cows_milk',
                    [],
                    'patient'
                ),
                $this->translator->trans(
                    'patient.fields.allergy.food_allergens.egg_protein',
                    [],
                    'patient'
                ) => $this->translator->trans('patient.fields.allergy.food_allergens.egg_protein', [], 'patient'),
                $this->translator->trans(
                    'patient.fields.allergy.food_allergens.egg_yolk',
                    [],
                    'patient'
                ) => $this->translator->trans(
                    'patient.fields.allergy.food_allergens.egg_yolk',
                    [],
                    'patient'
                ),
                $this->translator->trans(
                    'patient.fields.allergy.food_allergens.hake',
                    [],
                    'patient'
                ) => $this->translator->trans(
                    'patient.fields.allergy.food_allergens.hake',
                    [],
                    'patient'
                ),
                $this->translator->trans(
                    'patient.fields.allergy.food_allergens.orange',
                    [],
                    'patient'
                ) => $this->translator->trans(
                    'patient.fields.allergy.food_allergens.orange',
                    [],
                    'patient'
                ),
                $this->translator->trans(
                    'patient.fields.allergy.food_allergens.lemon',
                    [],
                    'patient'
                ) => $this->translator->trans(
                    'patient.fields.allergy.food_allergens.lemon',
                    [],
                    'patient'
                ),
                $this->translator->trans(
                    'patient.fields.allergy.food_allergens.mandarin',
                    [],
                    'patient'
                ) => $this->translator->trans(
                    'patient.fields.allergy.food_allergens.mandarin',
                    [],
                    'patient'
                ),
                $this->translator->trans(
                    'patient.fields.allergy.food_allergens.tomato',
                    [],
                    'patient'
                ) => $this->translator->trans(
                    'patient.fields.allergy.food_allergens.tomato',
                    [],
                    'patient'
                ),
                $this->translator->trans(
                    'patient.fields.allergy.food_allergens.carrot',
                    [],
                    'patient'
                ) => $this->translator->trans(
                    'patient.fields.allergy.food_allergens.carrot',
                    [],
                    'patient'
                ),
                $this->translator->trans(
                    'patient.fields.allergy.food_allergens.cacao',
                    [],
                    'patient'
                ) => $this->translator->trans(
                    'patient.fields.allergy.food_allergens.cacao',
                    [],
                    'patient'
                ),
                $this->translator->trans(
                    'patient.fields.allergy.food_allergens.raspberries',
                    [],
                    'patient'
                ) => $this->translator->trans('patient.fields.allergy.food_allergens.raspberries', [], 'patient'),
                $this->translator->trans(
                    'patient.fields.allergy.food_allergens.black_tea',
                    [],
                    'patient'
                ) => $this->translator->trans(
                    'patient.fields.allergy.food_allergens.black_tea',
                    [],
                    'patient'
                ),
            ],
            $this->translator->trans('patient.fields.allergy.fungal_allergens', [], 'patient') => [
                $this->translator->trans(
                    'patient.fields.allergy.fungal_allergens.alternaria_tenuis',
                    [],
                    'patient'
                ) => $this->translator->trans(
                    'patient.fields.allergy.fungal_allergens.alternaria_tenuis',
                    [],
                    'patient'
                ),
                $this->translator->trans(
                    'patient.fields.allergy.fungal_allergens.aspergillus_fumigatus',
                    [],
                    'patient'
                ) => $this->translator->trans(
                    'patient.fields.allergy.fungal_allergens.aspergillus_fumigatus',
                    [],
                    'patient'
                ),
                $this->translator->trans(
                    'patient.fields.allergy.fungal_allergens.cladosporium_herbarum',
                    [],
                    'patient'
                ) => $this->translator->trans(
                    'patient.fields.allergy.fungal_allergens.cladosporium_herbarum',
                    [],
                    'patient'
                ),
            ],
        ];

        // Build choice fields.
        switch ($field) {
            // Sex.
            case 'sex':
                $builder->add(
                    'sex',
                    'choice',
                    [
                        'choices' => [
                            $this->translator->trans(
                                'patient.fields.sex.male',
                                [],
                                'patient'
                            ) => 'patient.fields.sex.male',
                            $this->translator->trans(
                                'patient.fields.sex.female',
                                [],
                                'patient'
                            ) => 'patient.fields.sex.female',
                        ],
                        'label' => 'patient.fields.sex.label',
                        'translation_domain' => 'patient',
                        'required' => false,
                    ]
                );
                break;

            // Full term.
            case 'fullTerm':
                $builder->add(
                    'fullTerm',
                    'choice',
                    [
                        'choices' => [
                            1 => 'patient.fields.fullTerm.yes',
                            0 => 'patient.fields.fullTerm.no',
                        ],
                        'label' => 'patient.fields.fullTerm.label',
                        'translation_domain' => 'patient',
                        'required' => false,
                    ]
                );
                break;

            // Gestational age.
            case 'gestationalAge':
                $builder->add(
                    'gestationalAge',
                    'choice',
                    [
                        'choices' => array_combine(range(20, 38), range(20, 38)),
                        'label' => 'patient.fields.gestationalAge.label',
                        'required' => false,
                        'translation_domain' => 'patient',
                    ]
                );
                break;

            // Past illnesses.
            case 'pastIllnesses':
                $defaultOptions = [
                    $this->translator->trans(
                        'patient.fields.pastIllnesses.measles',
                        [],
                        'patient'
                    ) => $this->translator->trans(
                        'patient.fields.pastIllnesses.measles',
                        [],
                        'patient'
                    ),
                    $this->translator->trans(
                        'patient.fields.pastIllnesses.rubella',
                        [],
                        'patient'
                    ) => $this->translator->trans(
                        'patient.fields.pastIllnesses.rubella',
                        [],
                        'patient'
                    ),
                    $this->translator->trans(
                        'patient.fields.pastIllnesses.viral_hepatitis',
                        [],
                        'patient'
                    ) => $this->translator->trans(
                        'patient.fields.pastIllnesses.viral_hepatitis',
                        [],
                        'patient'
                    ),
                    $this->translator->trans(
                        'patient.fields.pastIllnesses.chickenpox',
                        [],
                        'patient'
                    ) => $this->translator->trans(
                        'patient.fields.pastIllnesses.chickenpox',
                        [],
                        'patient'
                    ),
                    $this->translator->trans(
                        'patient.fields.pastIllnesses.scarlet_fever',
                        [],
                        'patient'
                    ) => $this->translator->trans(
                        'patient.fields.pastIllnesses.scarlet_fever',
                        [],
                        'patient'
                    ),
                    $this->translator->trans(
                        'patient.fields.pastIllnesses.whooping_cough',
                        [],
                        'patient'
                    ) => $this->translator->trans(
                        'patient.fields.pastIllnesses.whooping_cough',
                        [],
                        'patient'
                    ),
                    $this->translator->trans(
                        'patient.fields.pastIllnesses.tuberculin_turn',
                        [],
                        'patient'
                    ) => $this->translator->trans(
                        'patient.fields.pastIllnesses.tuberculin_turn',
                        [],
                        'patient'
                    ),
                    $this->translator->trans(
                        'patient.fields.pastIllnesses.urti_monthly',
                        [],
                        'patient'
                    ) => $this->translator->trans(
                        'patient.fields.pastIllnesses.urti_monthly',
                        [],
                        'patient'
                    ),
                    $this->translator->trans(
                        'patient.fields.pastIllnesses.urti_3_to_4_times_per_year',
                        [],
                        'patient'
                    ) => $this->translator->trans(
                        'patient.fields.pastIllnesses.urti_3_to_4_times_per_year',
                        [],
                        'patient'
                    ),
                    $this->translator->trans(
                        'patient.fields.pastIllnesses.urti_with_bronchitis',
                        [],
                        'patient'
                    ) => $this->translator->trans(
                        'patient.fields.pastIllnesses.urti_with_bronchitis',
                        [],
                        'patient'
                    ),
                    $this->translator->trans(
                        'patient.fields.pastIllnesses.urti_with_acute_bronchitis',
                        [],
                        'patient'
                    ) => $this->translator->trans(
                        'patient.fields.pastIllnesses.urti_with_acute_bronchitis',
                        [],
                        'patient'
                    ),
                    $this->translator->trans(
                        'patient.fields.pastIllnesses.urti_uncomplicated',
                        [],
                        'patient'
                    ) => $this->translator->trans(
                        'patient.fields.pastIllnesses.urti_uncomplicated',
                        [],
                        'patient'
                    ),
                ];

                $builder->add(
                    'pastIllnesses',
                    'choice',
                    [
                        'choices' => empty($options) ? $defaultOptions + $fieldValuesFromDb : $options,
                        'label' => 'patient.fields.pastIllnesses.label',
                        'translation_domain' => 'patient',
                        'multiple' => true,
                        'required' => false,
                    ]
                );
                break;

            // Atopy.
            case 'atopy':
                $defaultOptions = [
                    $this->translator->trans(
                        'patient.fields.atopy.1_3_year',
                        [],
                        'patient'
                    ) => $this->translator->trans(
                        'patient.fields.atopy.1_3_year',
                        [],
                        'patient'
                    ),
                    $this->translator->trans('patient.fields.atopy.no', [], 'patient') => $this->translator->trans(
                        'patient.fields.atopy.no',
                        [],
                        'patient'
                    ),
                ];

                $builder->add(
                    'atopy',
                    'choice',
                    [
                        'choices' => empty($options) ? $defaultOptions + $fieldValuesFromDb : $options,
                        'label' => 'patient.fields.atopy.label',
                        'translation_domain' => 'patient',
                        'required' => false,
                    ]
                );
                break;

            // Allergey.
            case 'allergy':
                $defaultOptions = [
                        $this->translator->trans(
                            'patient.fields.allergy.alimentary_allergy',
                            [],
                            'patient'
                        ) => $this->translator->trans(
                            'alimentary allergy',
                            [],
                            'patient'
                        ),
                        $this->translator->trans(
                            'patient.fields.allergy.atopic_dermatitis',
                            [],
                            'patient'
                        ) => $this->translator->trans(
                            'atopic dermatitis',
                            [],
                            'patient'
                        ),
                        $this->translator->trans(
                            'patient.fields.allergy.rhinitis',
                            [],
                            'patient'
                        ) => $this->translator->trans(
                            'rhinitis',
                            [],
                            'patient'
                        ),
                        $this->translator->trans(
                            'patient.fields.allergy.bronchial_asthma',
                            [],
                            'patient'
                        ) => $this->translator->trans(
                            'bronchial asthma',
                            [],
                            'patient'
                        ),
                    ] + $allergyCategories;

                // Merge multi and single dimensional arrays.
                array_walk_recursive(
                    $defaultOptions,
                    function ($v, $k) use (&$fieldValuesFromDb) {
                        if (isset($fieldValuesFromDb[$k])) {
                            unset($fieldValuesFromDb[$k]);
                        }
                    }
                );

                $builder->add(
                    'allergy',
                    'choice',
                    [
                        'choices' => empty($options) ? $defaultOptions + $fieldValuesFromDb : $options,
                        'label' => 'patient.fields.allergy.label',
                        'multiple' => true,
                        'translation_domain' => 'patient',
                        'required' => false,
                    ]
                );
                break;

            // Sensibilization.
            case 'sensibilization':

                // Merge multi and single dimensional arrays.
                array_walk_recursive(
                    $allergyCategories,
                    function ($v, $k) use (&$fieldValuesFromDb) {
                        if (isset($fieldValuesFromDb[$k])) {
                            unset($fieldValuesFromDb[$k]);
                        }
                    }
                );

                $builder->add(
                    'sensibilization',
                    'choice',
                    [
                        'choices' => empty($options) ? $allergyCategories + $fieldValuesFromDb : $options,
                        'label' => 'patient.fields.sensibilization.label',
                        'multiple' => true,
                        'translation_domain' => 'patient',
                        'required' => false,
                    ]
                );
                break;

            // Treatment.
            case 'treatment':
                $defaultOptions = [
                    $this->translator->trans(
                        'patient.fields.treatment.sensibilization',
                        [],
                        'patient'
                    ) => $this->translator->trans(
                        'patient.fields.treatment.sensibilization',
                        [],
                        'patient'
                    ),
                    $this->translator->trans(
                        'patient.fields.treatment.inhaled_steroids',
                        [],
                        'patient'
                    ) => $this->translator->trans(
                        'patient.fields.treatment.inhaled_steroids',
                        [],
                        'patient'
                    ),
                    $this->translator->trans(
                        'patient.fields.treatment.systemic_steroids',
                        [],
                        'patient'
                    ) => $this->translator->trans(
                        'patient.fields.treatment.systemic_steroids',
                        [],
                        'patient'
                    ),
                    $this->translator->trans(
                        'patient.fields.treatment.alt',
                        [],
                        'patient'
                    ) => $this->translator->trans(
                        'patient.fields.treatment.alt',
                        [],
                        'patient'
                    ),
                    $this->translator->trans(
                        'patient.fields.treatment.ige_block',
                        [],
                        'patient'
                    ) => $this->translator->trans(
                        'patient.fields.treatment.ige_block',
                        [],
                        'patient'
                    ),
                    $this->translator->trans(
                        'patient.fields.treatment.gks_laba',
                        [],
                        'patient'
                    ) => $this->translator->trans(
                        'patient.fields.treatment.gks_laba',
                        [],
                        'patient'
                    ),
                ];

                $builder->add(
                    'treatment',
                    'choice',
                    [
                        'choices' => empty($options) ? $defaultOptions + $fieldValuesFromDb : $options,
                        'label' => 'patient.fields.treatment.label',
                        'multiple' => true,
                        'translation_domain' => 'patient',
                        'required' => false,
                    ]
                );
                break;

            // Disease.
            case 'disease':
                $defaultOptions = [];

                $builder->add(
                    'disease',
                    'choice',
                    [
                        'choices' => empty($options) ? $defaultOptions + $fieldValuesFromDb : $options,
                        'label' => 'patient.fields.disease.label',
                        'multiple' => true,
                        'translation_domain' => 'patient',
                        'required' => false,
                    ]
                );
                break;

            // Disease severity.
            case 'diseaseSeverity':
                $builder->add(
                    'diseaseSeverity',
                    'choice',
                    [
                        'choices' => [
                            1 => 1,
                            2 => 2,
                            3 => 3,
                            4 => 4,
                        ],
                        'label' => 'patient.fields.diseaseSeverity.label',
                        'translation_domain' => 'patient',
                        'required' => false,
                    ]
                );
                break;

            // Disease heredity.
            case 'heredity':
                $defaultOptions = [
                    $this->translator->trans(
                        'patient.fields.heredity.father',
                        [],
                        'patient'
                    ) => $this->translator->trans('patient.fields.heredity.father', [], 'patient'),
                    $this->translator->trans(
                        'patient.fields.heredity.mother',
                        [],
                        'patient'
                    ) => $this->translator->trans('patient.fields.heredity.mother', [], 'patient'),
                    $this->translator->trans('patient.fields.heredity.sips', [], 'patient') => $this->translator->trans(
                        'patient.fields.heredity.sips',
                        [],
                        'patient'
                    ),
                    $this->translator->trans(
                        'patient.fields.heredity.relatives',
                        [],
                        'patient'
                    ) => $this->translator->trans('patient.fields.heredity.relatives', [], 'patient'),
                ];

                $builder->add(
                    'heredity',
                    'choice',
                    [
                        'choices' => empty($options) ? $defaultOptions + $fieldValuesFromDb : $options,
                        'label' => 'patient.fields.heredity.label',
                        'translation_domain' => 'patient',
                        'multiple' => true,
                        'required' => false,
                    ]
                );
                break;

            // Medical examination.
            case 'examination':
                $defaultOptions = [
                    $this->translator->trans(
                        'patient.fields.examination.oak',
                        [],
                        'patient'
                    ) => $this->translator->trans('patient.fields.examination.oak', [], 'patient'),
                    $this->translator->trans(
                        'patient.fields.examination.oam',
                        [],
                        'patient'
                    ) => $this->translator->trans('patient.fields.examination.oam', [], 'patient'),
                    $this->translator->trans(
                        'patient.fields.examination.feces_scraping',
                        [],
                        'patient'
                    ) => $this->translator->trans('patient.fields.examination.feces_scraping', [], 'patient'),
                    $this->translator->trans(
                        'patient.fields.examination.blood_biochemistry',
                        [],
                        'patient'
                    ) => $this->translator->trans('patient.fields.examination.blood_biochemistry', [], 'patient'),
                    $this->translator->trans(
                        'patient.fields.examination.ig_e_common',
                        [],
                        'patient'
                    ) => $this->translator->trans('patient.fields.examination.ig_e_common', [], 'patient'),
                    $this->translator->trans(
                        'patient.fields.examination.immunogram',
                        [],
                        'patient'
                    ) => $this->translator->trans('patient.fields.examination.immunogram', [], 'patient'),
                    $this->translator->trans(
                        'patient.fields.examination.sweat_chloride',
                        [],
                        'patient'
                    ) => $this->translator->trans('patient.fields.examination.sweat_chloride', [], 'patient'),
                    $this->translator->trans(
                        'patient.fields.examination.allergy_testing',
                        [],
                        'patient'
                    ) => $this->translator->trans('patient.fields.examination.allergy_testing', [], 'patient'),
                    $this->translator->trans(
                        'patient.fields.examination.sputum_microscopy',
                        [],
                        'patient'
                    ) => $this->translator->trans('patient.fields.examination.sputum_microscopy', [], 'patient'),
                    $this->translator->trans(
                        'patient.fields.examination.bacteriological_sputum_culture',
                        [],
                        'patient'
                    ) => $this->translator->trans(
                        'patient.fields.examination.bacteriological_sputum_culture',
                        [],
                        'patient'
                    ),
                    $this->translator->trans(
                        'patient.fields.examination.bacteriological_culture_from_the_nasopharynx',
                        [],
                        'patient'
                    ) => $this->translator->trans(
                        'patient.fields.examination.bacteriological_culture_from_the_nasopharynx',
                        [],
                        'patient'
                    ),
                    $this->translator->trans(
                        'patient.fields.examination.electrocardiogram',
                        [],
                        'patient'
                    ) => $this->translator->trans('patient.fields.examination.electrocardiogram', [], 'patient'),
                    $this->translator->trans(
                        'patient.fields.examination.ultrasonography',
                        [],
                        'patient'
                    ) => $this->translator->trans('patient.fields.examination.ultrasonography', [], 'patient'),
                    $this->translator->trans(
                        'patient.fields.examination.ro_graphy',
                        [],
                        'patient'
                    ) => $this->translator->trans('patient.fields.examination.ro_graphy', [], 'patient'),
                    $this->translator->trans(
                        'patient.fields.examination.thermal_imaging',
                        [],
                        'patient'
                    ) => $this->translator->trans('patient.fields.examination.thermal_imaging', [], 'patient'),
                    $this->translator->trans(
                        'patient.fields.examination.spirography',
                        [],
                        'patient'
                    ) => $this->translator->trans('patient.fields.examination.spirography', [], 'patient'),
                    $this->translator->trans(
                        'patient.fields.examination.peak_flow_meter',
                        [],
                        'patient'
                    ) => $this->translator->trans('patient.fields.examination.peak_flow_meter', [], 'patient'),
                    $this->translator->trans(
                        'patient.fields.examination.bronchodilator_test',
                        [],
                        'patient'
                    ) => $this->translator->trans('patient.fields.examination.bronchodilator_test', [], 'patient'),
                    $this->translator->trans(
                        'patient.fields.examination.ent_consultation',
                        [],
                        'patient'
                    ) => $this->translator->trans('patient.fields.examination.ent_consultation', [], 'patient'),
                    $this->translator->trans(
                        'patient.fields.examination.consultation_of_other_specialists',
                        [],
                        'patient'
                    ) => $this->translator->trans(
                        'patient.fields.examination.consultation_of_other_specialists',
                        [],
                        'patient'
                    ),
                ];

                $builder->add(
                    'examination',
                    'choice',
                    [
                        'choices' => empty($options) ? $defaultOptions + $fieldValuesFromDb : $options,
                        'label' => 'patient.fields.examination.label',
                        'translation_domain' => 'patient',
                        'multiple' => true,
                        'required' => false,
                    ]
                );

                break;

            // Accompanying illnesses.
            case 'accompanyingIllnesses':
                $defaultOptions = [];

                $builder->add(
                    'accompanyingIllnesses',
                    'choice',
                    [
                        'choices' => empty($options) ? $defaultOptions + $fieldValuesFromDb : $options,
                        'label' => 'patient.fields.accompanyingIllnesses.label',
                        'translation_domain' => 'patient',
                        'multiple' => true,
                        'required' => false,
                    ]
                );
                break;
        }
    }
}