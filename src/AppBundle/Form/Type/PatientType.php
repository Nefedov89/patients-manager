<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class PatientType
 * @package AppBundle\Form\Type
 */
class PatientType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', null, ['label' => 'patient.fields.first_name.label', 'translation_domain' => 'patient'])
            ->add('lastName', null, ['label' => 'patient.fields.last_name.label', 'translation_domain' => 'patient'])
            ->add(
                'middleName',
                null,
                ['label' => 'patient.fields.middle_name.label', 'translation_domain' => 'patient', 'required' => false]
            )
            ->add(
                'birthday',
                'birthday',
                [
                    'widget' => 'single_text',
                    'label' => 'patient.fields.birthday.label',
                    'translation_domain' => 'patient',
                ]
            )
            ->add(
                'email',
                'email',
                ['label' => 'patient.fields.email.label', 'translation_domain' => 'patient', 'required' => false]
            )
            ->add(
                'phone',
                null,
                ['label' => 'patient.fields.phone.label', 'translation_domain' => 'patient', 'required' => false]
            )
            ->add(
                'address',
                null,
                ['label' => 'patient.fields.address.label', 'translation_domain' => 'patient', 'required' => false]
            )
            ->add(
                'commonData',
                null,
                ['label' => 'patient.fields.commonData.label', 'translation_domain' => 'patient', 'required' => false]
            )
            ->add(
                'diseaseDate',
                'hidden'
            )
            ->add(
                'pastIllnessesDate',
                'hidden'
            )
            ->add(
                'allergyDate',
                'hidden'
            )
            ->add(
                'heredityDisease',
                'hidden'
            )
            ->add(
                'examinationDate',
                'hidden'
            )
            ->add(
                'filters',
                'appbundle_patient_filters',
                [
                    'data' => $options['data'],
                ]
            );
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => 'AppBundle\Entity\Patient',
                'validation_groups' => ['edit', 'create'],
            ]
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_patient';
    }
}
