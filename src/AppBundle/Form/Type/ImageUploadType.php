<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class ImageUploadType
 * @package AppBundle\Form\Type
 */
class ImageUploadType extends AbstractType
{

    /**
     * @return string
     */
    public function getParent()
    {
        return 'sonata_media_type';
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            [
                'label' => 'Image',
                'required' => false,
                'cascade_validation' => true,
                'context' => 'default',
                'provider' => 'sonata.media.provider.image',
            ]
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'app_bundle_image_upload';
    }
}