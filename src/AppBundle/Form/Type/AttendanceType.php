<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class AttendanceType
 * @package AppBundle\Form\Type
 */
class AttendanceType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('reason', null, ['label' => 'attendance.fields.reason.label', 'translation_domain' => 'attendance'])
            ->add(
                'date',
                'date',
                [
                    'widget' => 'single_text',
                    'label' => 'attendance.fields.date.label',
                    'translation_domain' => 'attendance',
                ]
            )
            ->add(
                'description',
                null,
                [
                    'label' => 'attendance.fields.description.label',
                    'translation_domain' => 'attendance',
                    'required' => false,
                ]
            )
            ->add(
                'therapy',
                null,
                [
                    'label' => 'attendance.fields.therapy.label',
                    'translation_domain' => 'attendance',
                    'required' => false,
                ]
            )
            ->add(
                'isAttended',
                'checkbox',
                [
                    'label' => 'attendance.fields.is_attended.label',
                    'translation_domain' => 'attendance',
                    'required' => false,
                ]
            );
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => 'AppBundle\Entity\Attendance',
                'validation_groups' => ['edit', 'create'],
            ]
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_attendance';
    }
}
