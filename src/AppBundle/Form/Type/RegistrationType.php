<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class RegistrationType
 * @package AppBundle\Form
 */
class RegistrationType extends AbstractType
{

    /**
     * Method for building registration form.
     *
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

    }

    /**
     * Method for retrieving parent registration form.
     *
     * @return string
     */
    public function getParent()
    {
        return 'fos_user_registration';
    }

    /**
     * Method for retrieving unique name of custom registration form.
     *
     * @return string
     */
    public function getName()
    {
        return 'app_bundle_user_register';
    }
}
