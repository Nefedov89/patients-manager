<?php

namespace AppBundle\Controller;

use AppBundle\Form\Type\PatientType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use AppBundle\Entity\Patient;
use Symfony\Component\HttpFoundation\Response;

/**
 * Patient controller.
 *
 * @Route("/patient")
 *
 * Class PatientController
 * @package AppBundle\Controller
 */
class PatientController extends Controller
{
    /**
     * Creates a new Patient entity.
     *
     * @Route("/", name="patient_create")
     * @Method("POST")
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function createAction(Request $request)
    {
        $entity = new Patient();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);
        $user = $this->getUser();

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            // Set related user.
            $entity->setUser($user);
            $em->flush();

            // Set flash message.
            $this->addFlash(
                'notice',
                $this->get('translator')->trans(
                    'Patient %firstName% %lastName% was successfully created',
                    [
                        '%firstName%' => $entity->getFirstName(),
                        '%lastName%' => $entity->getLastName(),
                    ]
                )
            );

            return $this->redirect($this->generateCrudRedirectUrl());
        }

        return $this->render(
            'AppBundle:Patient:new.html.twig',
            [
                'entity' => $entity,
                'form' => $form->createView(),
                'action' => $this->generateUrl('patient_create'),
                'method' => 'POST',
            ]
        );
    }

    /**
     * Displays a form to create a new Patient entity.
     * @Route("/patient-new", name="patient_new")
     * @Method("GET")
     * @Security("has_role('ROLE_DOCTOR') or has_role('ROLE_ADMIN')")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newAction()
    {
        $entity = new Patient();
        $form = $this->createCreateForm($entity);

        return $this->render(
            'AppBundle:Patient:new.html.twig',
            [
                'method' => 'POST',
                'action' => $this->generateUrl('patient_create'),
                'entity' => $entity,
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * Finds and displays a Patient entity.
     *
     * @Route("/{id}", name="patient_show")
     * @Method("GET")
     * @ParamConverter("patient", class="AppBundle:Patient")
     *
     * @param \AppBundle\Entity\Patient $entity
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(Patient $entity)
    {
        if (!$entity) {
            throw $this->createNotFoundException($this->get('translator')->trans('Unable to find Patient entity.'));
        }

        $deleteForm = $this->createDeleteForm($entity->getId());

        return $this->render(
            'AppBundle:Patient:show.html.twig',
            [
                'entity' => $entity,
                'delete_form' => $deleteForm->createView(),
            ]
        );
    }

    /**
     * Displays a form to edit an existing Patient entity.
     *
     * @Route("/{id}/edit", name="patient_edit")
     * @Method("GET")
     * @ParamConverter("patient", class="AppBundle:Patient")
     * @Security("patient.isDoctor(user) or has_role('ROLE_ADMIN')")
     *
     * @param \AppBundle\Entity\Patient $entity
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Patient $entity)
    {
        if (!$entity) {
            throw $this->createNotFoundException($this->get('translator')->trans('Unable to find Patient entity.'));
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($entity->getId());

        return $this->render(
            'AppBundle:Patient:edit.html.twig',
            [
                'entity' => $entity,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            ]
        );
    }

    /**
     * Edits an existing Patient entity.
     *
     * @Route("/{id}", name="patient_update")
     * @Method("PUT")
     * @ParamConverter("patient", class="AppBundle:Patient")
     * @Security("patient.isDoctor(user) or has_role('ROLE_ADMIN')")
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param \AppBundle\Entity\Patient                 $entity
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function updateAction(Request $request, Patient $entity)
    {
        $em = $this->getDoctrine()->getManager();

        if (!$entity) {
            throw $this->createNotFoundException($this->get('translator')->trans('Unable to find Patient entity.'));
        }

        $deleteForm = $this->createDeleteForm($entity->getId());
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            // Set flash message.
            $this->addFlash(
                'notice',
                $this->get('translator')->trans(
                    'Patient %firstName% %lastName% was successfully updated',
                    [
                        '%firstName%' => $entity->getFirstName(),
                        '%lastName%' => $entity->getLastName(),
                    ]
                )
            );

            return $this->redirect($this->generateCrudRedirectUrl());
        }

        return $this->render(
            'AppBundle:Patient:edit.html.twig',
            [
                'entity' => $entity,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            ]
        );
    }

    /**
     * Deletes a Patient entity.
     *
     * @Route("/{id}", name="patient_delete")
     * @Method("DELETE")
     * @ParamConverter("patient", class="AppBundle:Patient")
     * @Security("patient.isDoctor(user) or has_role('ROLE_ADMIN')")
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param \AppBundle\Entity\Patient                 $entity
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, Patient $entity)
    {
        $form = $this->createDeleteForm($entity->getId());
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            if (!$entity) {
                throw $this->createNotFoundException($this->get('translator')->trans('Unable to find Patient entity.'));
            }

            $em->remove($entity);
            $em->flush();

            // Set flash message.
            $this->addFlash(
                'notice',
                $this->get('translator')->trans(
                    'Patient %firstName% %lastName% was successfully deleted',
                    [
                        '%firstName%' => $entity->getFirstName(),
                        '%lastName%' => $entity->getLastName(),
                    ]
                )
            );
        }

        return $this->redirect($this->generateCrudRedirectUrl());
    }

    /**
     * Generate CRUD redirect URL for Patient's dashboard.
     *
     * @return string Redirect URL
     */
    public function generateCrudRedirectUrl()
    {
        $user = $this->getUser();
        $redirectRoute = 'admin_patient_entity_list';

        if ($user->hasRole('ROLE_DOCTOR')) {
            $redirectRoute = 'dashboard';
        }

        return $this->generateUrl($redirectRoute);
    }

    /**
     * Get HTML for patient child field (Disease term, heredity disease, medical examination year, ...).
     *
     * @Route("/child-field-markup", name="patient_child_field_markup", options={"expose"=true})
     * @Method("POST")
     * @param Request $request
     *
     * @return string
     */
    public function getPatientChildField(Request $request)
    {
        if ($request->isXmlHttpRequest()) {
            $markup = $this->renderView(
                'AppBundle:Patient/Partials:_filters_child_field.html.twig',
                [
                    'defaultValue' => $request->get('defaultValue'),
                    'parentValuesItem' => $request->get('parentValuesItem'),
                    'field' => $request->get('field'),
                    'index' => $request->get('index'),
                ]
            );

            return new Response($markup);
        }

        return new Response($this->get('translator')->trans('patient.child_fields.no_ajax.msg', [], 'patient'), 400);
    }

    /**
     * Creates a form to create a Patient entity.
     *
     * @param \AppBundle\Entity\Patient $entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Patient $entity)
    {
        $form = $this->createForm(
            new PatientType(),
            $entity,
            [
                'action' => $this->generateUrl('patient_create'),
                'method' => 'POST',
            ]
        );

        $form->add('submit', 'submit', ['label' => 'Create']);

        return $form;
    }

    /**
     * Creates a form to delete a Patient entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('patient_delete', ['id' => $id]))
            ->setMethod('DELETE')
            ->add('submit', 'submit', ['label' => 'Delete', 'attr' => ['class' => 'btn btn-danger entity-del']])
            ->getForm();
    }

    /**
     * Creates a form to edit a Patient entity.
     *
     * @param \AppBundle\Entity\Patient $entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Patient $entity)
    {
        $form = $this->createForm(
            new PatientType(),
            $entity,
            [
                'action' => $this->generateUrl('patient_update', ['id' => $entity->getId()]),
                'method' => 'PUT',
            ]
        );

        $form->add('submit', 'submit', ['label' => 'Update']);

        return $form;
    }
}
