<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Patient;
use AppBundle\Entity\PatientRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
 * Dashboard controller.
 *
 * Class DashboardController
 * @package AppBundle\Controller
 */
class DashboardController extends Controller
{
    /**
     * Dashboard page.
     * Show current user patients.
     *
     * @Route("/", name="dashboard")
     * @param Request $request
     *
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $user = $this->getUser();

        // Admin homepage.
        if ($user->hasRole('ROLE_ADMIN')) {
            $em = $this->getDoctrine()->getManager();
            $userRepository = $em->getRepository('AppBundle:User');

            return $this->render(
                'AppBundle:Admin:Dashboard/index.html.twig',
                [
                    'usersCount' => $userRepository->findAllCount(),
                ]
            );
        }

        return $this->render(
            'AppBundle:Dashboard:index.html.twig',
            [
                'patients' => $user->getPatients(),
            ]
        );
    }

    /**
     * List of all attendances for particular user(doctor).
     *
     * @Route("/attendances", name="attendances_list")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function attendancesAction()
    {
        $user = $this->getUser();

        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('AppBundle:Attendance');

        return $this->render(
            'AppBundle:Dashboard:all_attendances.html.twig',
            [
                'allAttendances' => $repository->findTimedAttendances($user),
                'pastAttendances' => $repository->findTimedAttendances($user, 'past'),
                'futureAttendances' => $repository->findTimedAttendances($user, 'future'),
                'todayAttendances' => $repository->findTimedAttendances($user, 'today'),
                'notAttended' => $repository->findAttendedVisits($user, 0),
            ]
        );
    }

    /**
     * List of attendances for particular patient.
     *
     * @Route("/attendances/{id}", name="patient_attendances_list")
     *
     * @ParamConverter("patient", class="AppBundle:Patient")
     * @Security("patient.isDoctor(user) or has_role('ROLE_ADMIN')")
     *
     * @param \AppBundle\Entity\Patient $patient
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function patientAttendancesAction(Patient $patient)
    {
        return $this->render(
            'AppBundle:Dashboard:patient_attendances.html.twig',
            [
                'entities' => $patient->getAttendances(),
                'patient' => $patient,
            ]
        );
    }

    /**
     * Patients filters.
     *
     * @Route("/patients-filters", name="patients_filters")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function patientFiltersAction(Request $request)
    {
        $user = $this->getUser();

        $em = $this->getDoctrine()->getManager();
        /** @var PatientRepository $repository */
        $repository = $em->getRepository('AppBundle:Patient');
        $patients = $repository->findByHospital($user->getHospital());
        $filtersForm = $this->getPatientsFiltersForm();
        $filterParamsStr = '';
        $clearBtn = false;

        // Handle filter request.
        $filtersForm->handleRequest($request);

        if ($filtersForm->isValid()) {
            $data = $filtersForm->getData();

            foreach ($data as $k => $v) {
                if (is_null($v) || (is_array($v) && empty($v))) {
                    unset($data[$k]);
                } else {
                    $valueParamsStr = is_array($v) ? implode(', ', $v) : $v;
                    $filterParamsStr .= $this->get('translator')->trans(
                        /** @Ignore */
                        $filtersForm->get($k)->getConfig()->getOption('label'),
                        [],
                        'patient'
                    ).': '.$valueParamsStr.' | ';
                }
            }

            // Get filtered patients.
            $patients = $repository->findByFilters($data, $user->getHospital());

            // Update filters form. Set selected values and default options.
            $filtersForm = $this->getPatientsFiltersForm($data);

            // Set flash message.
            if ($filterParamsStr) {
                $this->addFlash(
                    'info',
                    $this->get('translator')->trans(
                        'patient_filters.filter_params_msg',
                        [
                            '%filters%' => $filterParamsStr,
                        ],
                        'dashboard'
                    )
                );

                $clearBtn = true;
            }
        }

        return $this->render(
            'AppBundle:Dashboard:patient_filters.html.twig',
            [
                'patients' => $patients,
                'filtersForm' => $filtersForm->createView(),
                'clearBtn' => $clearBtn,
            ]
        );
    }

    /**
     * Get patients filters form.
     *
     * @param null $data
     *
     * @return \Symfony\Component\Form\Form
     */
    private function getPatientsFiltersForm($data = null)
    {
        $filtersForm = $this->createForm(
            'appbundle_patient_filters',
            $data,
            [
                'action' => $this->generateUrl('patients_filters'),
            ]
        );

        $filtersForm->add(
            'submit',
            'submit',
            ['label' => $this->get('translator')->trans('patient_filters.filter_btn', [], 'dashboard')]
        );

        return $filtersForm;
    }
}
