<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use AppBundle\Entity\Department;
use AppBundle\Form\Type\DepartmentType;

/**
 * Department controller.
 *
 * @Route("/department")
 *
 * @Security("has_role('ROLE_ADMIN')")
 *
 * Class DepartmentController
 * @package AppBundle\Controller
 */
class DepartmentController extends Controller
{
    /**
     * Creates a new Department entity.
     *
     * @Route("/", name="department_create")
     * @Method("POST")
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function createAction(Request $request)
    {
        $entity = new Department();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            // Set flash message.
            $this->addFlash(
                'notice',
                $this->get('translator')->trans(
                    'Department %name% was successfully created',
                    ['%name%' => $entity->getName()]
                )
            );

            return $this->redirect($this->generateUrl('admin_department_entity_list'));
        }

        return $this->render(
            'AppBundle:Department:new.html.twig',
            [
                'entity' => $entity,
                'form' => $form->createView(),
                'action' => $this->generateUrl('department_create'),
                'method' => 'POST',
            ]
        );
    }

    /**
     * Displays a form to create a new Department entity.
     * @Route("/department-new", name="department_new")
     * @Method("GET")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newAction()
    {
        $entity = new Department();
        $form = $this->createCreateForm($entity);

        return $this->render(
            'AppBundle:Department:new.html.twig',
            [
                'method' => 'POST',
                'action' => $this->generateUrl('department_create'),
                'entity' => $entity,
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * Finds and displays a Department entity.
     *
     * @Route("/{id}", name="department_show")
     * @Method("GET")
     *
     * @param integer $id
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:Department')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException($this->get('translator')->trans('Unable to find Department entity.'));
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render(
            'AppBundle:Department:show.html.twig',
            [
                'entity' => $entity,
                'delete_form' => $deleteForm->createView(),
            ]
        );
    }

    /**
     * Displays a form to edit an existing Department entity.
     *
     * @Route("/{id}/edit", name="department_edit")
     * @Method("GET")
     *
     * @param integer $id
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:Department')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException($this->get('translator')->trans('Unable to find Department entity.'));
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render(
            'AppBundle:Department:edit.html.twig',
            [
                'entity' => $entity,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            ]
        );
    }

    /**
     * Edits an existing Department entity.
     *
     * @Route("/{id}", name="department_update")
     * @Method("PUT")
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param integer                                   $id
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:Department')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException($this->get('translator')->trans('Unable to find Department entity.'));
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            // Set flash message.
            $this->addFlash(
                'notice',
                $this->get('translator')->trans(
                    'Department %name% was successfully updated',
                    ['%name%' => $entity->getName()]
                )
            );

            return $this->redirect($this->generateUrl('admin_department_entity_list', ['id' => $id]));
        }

        return $this->render(
            'AppBundle:Department:edit.html.twig',
            [
                'entity' => $entity,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            ]
        );
    }

    /**
     * Deletes a Department entity.
     *
     * @Route("/{id}", name="department_delete")
     * @Method("DELETE")
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param integer                                   $id
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:Department')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException(
                    $this->get('translator')->trans('Unable to find Department entity.')
                );
            }

            $em->remove($entity);
            $em->flush();

            // Set flash message.
            $this->addFlash(
                'notice',
                $this->get('translator')->trans(
                    'Department %name% was successfully deleted',
                    ['%name%' => $entity->getName()]
                )
            );
        }

        return $this->redirect($this->generateUrl('admin_department_entity_list'));
    }

    /**
     * Creates a form to delete a Department entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('department_delete', ['id' => $id]))
            ->setMethod('DELETE')
            ->add('submit', 'submit', ['label' => 'Delete', 'attr' => ['class' => 'btn btn-danger entity-del']])
            ->getForm();
    }

    /**
     * Creates a form to create a Department entity.
     *
     * @param Department $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Department $entity)
    {
        $form = $this->createForm(
            new DepartmentType(),
            $entity,
            [
                'action' => $this->generateUrl('department_create'),
                'method' => 'POST',
            ]
        );

        $form->add('submit', 'submit', ['label' => 'Create']);

        return $form;
    }

    /**
     * Creates a form to edit a Department entity.
     *
     * @param Department $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Department $entity)
    {
        $form = $this->createForm(
            new DepartmentType(),
            $entity,
            [
                'action' => $this->generateUrl('department_update', ['id' => $entity->getId()]),
                'method' => 'PUT',
            ]
        );

        $form->add('submit', 'submit', ['label' => 'Update']);

        return $form;
    }
}
