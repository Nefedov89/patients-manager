<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use AppBundle\Entity\User;
use AppBundle\Form\Type\UserType;

/**
 * User controller.
 *
 * @Route("/user")
 *
 * @Security("has_role('ROLE_ADMIN')")
 *
 * Class UserController
 * @package AppBundle\Controller
 */
class UserController extends Controller
{
    const DEFAULT_PASS = 'pass';

    /**
     *  Creates a new User entity.
     *
     * @Route("/", name="user_create")
     * @Method("POST")
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function createAction(Request $request)
    {
        $entity = new User();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);
        $formData = $form->getData();

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            // Set default pass if null.
            if (is_null($formData->getPlainPassword())) {
                $entity->setPlainPassword(self::DEFAULT_PASS);
                $this->get('fos_user.user_manager')->updatePassword($entity);
            }

            // Create confirmation token.
            $token = sha1(uniqid(mt_rand(), true));
            $entity->setConfirmationToken($token);

            $em->persist($entity);
            $em->flush();

            // Send confirmation message.
            $mailer = $this->container->get('fos_user.mailer');
            $mailer->sendConfirmationEmailMessage($entity);

            // Set flash message.
            $this->addFlash(
                'notice',
                $this->get('translator')->trans(
                    'User %name% was successfully created',
                    [
                        '%name%' => $entity->getUsername(),
                    ]
                )
            );

            return $this->redirect($this->generateUrl('admin_user_entity_list'));
        }

        return $this->render(
            'AppBundle:User:new.html.twig',
            [
                'entity' => $entity,
                'form' => $form->createView(),
                'action' => $this->generateUrl('user_create'),
                'method' => 'POST',
            ]
        );
    }

    /**
     * Displays a form to create a new User entity.
     * @Route("/user-new", name="user_new")
     * @Method("GET")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newAction()
    {
        $entity = new User();
        $form = $this->createCreateForm($entity);

        return $this->render(
            'AppBundle:User:new.html.twig',
            [
                'method' => 'POST',
                'action' => $this->generateUrl('user_create'),
                'entity' => $entity,
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * Finds and displays a User entity.
     *
     * @Route("/{id}", name="user_show")
     * @Method("GET")
     *
     * @param integer $id
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:User')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException($this->get('translator')->trans('Unable to find User entity.'));
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render(
            'AppBundle:User:show.html.twig',
            [
                'entity' => $entity,
                'delete_form' => $deleteForm->createView(),
            ]
        );
    }

    /**
     * Displays a form to edit an existing User entity.
     *
     * @Route("/{id}/edit", name="user_edit")
     * @Method("GET")
     *
     * @param integer $id
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:User')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException($this->get('translator')->trans('Unable to find User entity.'));
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render(
            'AppBundle:User:edit.html.twig',
            [
                'entity' => $entity,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            ]
        );
    }

    /**
     * Edits an existing User entity.
     *
     * @Route("/{id}", name="user_update")
     * @Method("PUT")
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param integer                                   $id
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:User')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException($this->get('translator')->trans('Unable to find User entity.'));
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            // Update user entity.
            $this->get('fos_user.user_manager')->updateUser($entity);

            // Set flash message.
            $this->addFlash(
                'notice',
                $this->get('translator')->trans(
                    'User %name% was successfully updated',
                    [
                        '%name%' => $entity->getUsername(),
                    ]
                )
            );

            return $this->redirect($this->generateUrl('admin_user_entity_list'));
        }

        return $this->render(
            'AppBundle:User:edit.html.twig',
            [
                'entity' => $entity,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            ]
        );
    }

    /**
     * Deletes a User entity.
     *
     * @Route("/{id}", name="user_delete")
     * @Method("DELETE")
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param integer                                   $id
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:User')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException($this->get('translator')->trans('Unable to find User entity.'));
            }

            $em->remove($entity);
            $em->flush();

            // Set flash message.
            $this->addFlash(
                'notice',
                $this->get('translator')->trans(
                    'User %name% was successfully deleted',
                    [
                        '%name%' => $entity->getUsername(),
                    ]
                )
            );
        }

        return $this->redirect($this->generateUrl('admin_user_entity_list'));
    }

    /**
     * Creates a form to delete a User entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('user_delete', ['id' => $id]))
            ->setMethod('DELETE')
            ->add('submit', 'submit', ['label' => 'Delete', 'attr' => ['class' => 'btn btn-danger entity-del']])
            ->getForm();
    }

    /**
     * Creates a form to create a User entity.
     *
     * @param User $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(User $entity)
    {
        $form = $this->createForm(
            new UserType($this->container),
            $entity,
            [
                'action' => $this->generateUrl('user_create'),
                'method' => 'POST',
            ]
        );

        return $form;
    }

    /**
     * Creates a form to edit a User entity.
     *
     * @param User $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(User $entity)
    {
        $form = $this->createForm(
            new UserType(),
            $entity,
            [
                'action' => $this->generateUrl('user_update', ['id' => $entity->getId()]),
                'method' => 'PUT',
            ]
        );

        $form->add('submit', 'submit', ['label' => 'Update']);

        return $form;
    }
}
