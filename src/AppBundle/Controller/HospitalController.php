<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use AppBundle\Entity\Hospital;
use AppBundle\Form\Type\HospitalType;

/**
 * Hospital controller.
 *
 * @Route("/hospital")
 *
 * @Security("has_role('ROLE_ADMIN')")
 *
 * Class HospitalController
 * @package AppBundle\Controller
 */
class HospitalController extends Controller
{
    /**
     * Creates a new Hospital entity.
     *
     * @Route("/", name="hospital_create")
     * @Method("POST")
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function createAction(Request $request)
    {
        $entity = new Hospital();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            // Set flash message.
            $this->addFlash(
                'notice',
                $this->get('translator')->trans(
                    'Hospital %name% was successfully created',
                    ['%name%' => $entity->getName()]
                )
            );

            return $this->redirect($this->generateUrl('admin_hospital_entity_list'));
        }

        return $this->render(
            'AppBundle:Hospital:new.html.twig',
            [
                'entity' => $entity,
                'form' => $form->createView(),
                'action' => $this->generateUrl('hospital_create'),
                'method' => 'POST',
            ]
        );
    }

    /**
     * Displays a form to create a new Hospital entity.
     * @Route("/hospital-new", name="hospital_new")
     * @Method("GET")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newAction()
    {
        $entity = new Hospital();
        $form = $this->createCreateForm($entity);

        return $this->render(
            'AppBundle:Hospital:new.html.twig',
            [
                'method' => 'POST',
                'action' => $this->generateUrl('hospital_create'),
                'entity' => $entity,
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * Finds and displays a Hospital entity.
     *
     * @Route("/{id}", name="hospital_show")
     * @Method("GET")
     *
     * @param integer $id
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:Hospital')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException($this->get('translator')->trans('Unable to find Hospital entity.'));
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render(
            'AppBundle:Hospital:show.html.twig',
            [
                'entity' => $entity,
                'delete_form' => $deleteForm->createView(),
            ]
        );
    }

    /**
     * Displays a form to edit an existing Hospital entity.
     *
     * @Route("/{id}/edit", name="hospital_edit")
     * @Method("GET")
     *
     * @param integer $id
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:Hospital')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException($this->get('translator')->trans('Unable to find Hospital entity.'));
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render(
            'AppBundle:Hospital:edit.html.twig',
            [
                'entity' => $entity,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            ]
        );
    }

    /**
     * Edits an existing Hospital entity.
     *
     * @Route("/{id}", name="hospital_update")
     * @Method("PUT")
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param integer                                   $id
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:Hospital')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException($this->get('translator')->trans('Unable to find Hospital entity.'));
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            // Set flash message.
            $this->addFlash(
                'notice',
                $this->get('translator')->trans(
                    'Hospital %name% was successfully updated',
                    ['%name%' => $entity->getName()]
                )
            );

            return $this->redirect($this->generateUrl('admin_hospital_entity_list'));
        }

        return $this->render(
            'AppBundle:Hospital:edit.html.twig',
            [
                'entity' => $entity,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            ]
        );
    }

    /**
     * Deletes a Hospital entity.
     *
     * @Route("/{id}", name="hospital_delete")
     * @Method("DELETE")
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param integer                                   $id
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:Hospital')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException(
                    $this->get('translator')->trans('Unable to find Hospital entity.')
                );
            }

            $em->remove($entity);
            $em->flush();

            // Set flash message.
            $this->addFlash(
                'notice',
                $this->get('translator')->trans(
                    'Hospital %name% was successfully deleted',
                    ['%name%' => $entity->getName()]
                )
            );
        }

        return $this->redirect($this->generateUrl('admin_hospital_entity_list'));
    }

    /**
     * Creates a form to delete a Hospital entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('hospital_delete', ['id' => $id]))
            ->setMethod('DELETE')
            ->add('submit', 'submit', ['label' => 'Delete', 'attr' => ['class' => 'btn btn-danger entity-del']])
            ->getForm();
    }

    /**
     * Creates a form to create a Hospital entity.
     *
     * @param Hospital $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Hospital $entity)
    {
        $form = $this->createForm(
            new HospitalType(),
            $entity,
            [
                'action' => $this->generateUrl('hospital_create'),
                'method' => 'POST',
            ]
        );

        $form->add('submit', 'submit', ['label' => 'Create']);

        return $form;
    }

    /**
     * Creates a form to edit a Hospital entity.
     *
     * @param Hospital $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Hospital $entity)
    {
        $form = $this->createForm(
            new HospitalType(),
            $entity,
            [
                'action' => $this->generateUrl('hospital_update', ['id' => $entity->getId()]),
                'method' => 'PUT',
            ]
        );

        $form->add('submit', 'submit', ['label' => 'Update']);

        return $form;
    }
}
