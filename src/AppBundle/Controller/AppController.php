<?php

/**
 * @file
 *
 * Common application tasks.
 */

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * App controller for general purposes.
 *
 * Class AppController
 * @package AppBundle\Controller
 */
class AppController extends Controller
{
    const AJAX_STATUS_BAD = 422;
    const AJAX_STATUS_OK = 200;

    /**
     * Contact us form processing.
     *
     * @Route("/contact-us", name="contact_us", options={"expose"=true})
     * @Method("POST")
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function contactUs(Request $request)
    {
        $errorsBag = [];
        $validator = $this->get('validator');
        $formData = $request->request->get('formData');
        $validationMap = [
            'email' => [
                new Email(),
                new NotBlank(),
            ],
            'message' => [
                new NotBlank(),
                new Length(['min' => 5]),
            ],
        ];

        // Collect errors messages.
        foreach ($validationMap as $field => $constraints) {
            $errors = $validator->validate($formData[$field], $constraints);
            foreach ($errors as $error) {
                $errorsBag[$field] = $error->getMessage();
            }
        }

        $statusCode = count($errorsBag) == 0 ? self::AJAX_STATUS_OK : self::AJAX_STATUS_BAD;

        // Send email for admin.
        if ($statusCode == self::AJAX_STATUS_OK) {
            $message = \Swift_Message::newInstance()
                ->setTo($this->container->getParameter('mailer_contact_us_to'))
                ->setSubject($this->get('translator')->trans('Contact us form submission'))
                ->setFrom($formData['email'])
                ->setBody(
                    $this->renderView(
                        'AppBundle:Emails:contact.html.twig',
                        [
                            'email' => $formData['email'],
                            'message' => $formData['message'],
                        ]
                    ),
                    'text/html'
                );

            $this->get('mailer')->send($message);

            // Set success flash message.
            $this->addFlash(
                'notice',
                $this->get('translator')->trans('Thank you! We will contact you as soon as possible')
            );
        }

        return new JsonResponse(
            [
                'errors' => $errorsBag,
            ],
            $statusCode
        );
    }
}
