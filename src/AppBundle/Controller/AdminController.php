<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Admin controller.
 *
 * @Route("/admin")
 *
 * Class AdminController
 * @package AppBundle\Controller
 */
class AdminController extends Controller
{
    /**
     * Admin attendances dashboard.
     *
     * @Route("/entity/attendance", name="admin_attendance_entity_list")
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function attendancesAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('AppBundle:Attendance');
        $entities = $repository->findAll();

        return $this->render(
            'AppBundle:Admin/Entities/Attendance:entity_table_attendance.html.twig',
            [
                'entities' => $entities,
            ]
        );
    }

    /**
     * Admin departments dashboard.
     *
     * @Route("/entity/department", name="admin_department_entity_list")
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function departmentsAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('AppBundle:Department');
        $entities = $repository->findAll();

        return $this->render(
            'AppBundle:Admin/Entities/Department:entity_table_department.html.twig',
            [
                'entities' => $entities,
            ]
        );
    }

    /**
     * Admin hospitals dashboard.
     *
     * @Route("/entity/hospital", name="admin_hospital_entity_list")
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function hospitalsAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('AppBundle:Hospital');
        $entities = $repository->findAll();

        return $this->render(
            'AppBundle:Admin/Entities/Hospital:entity_table_hospital.html.twig',
            [
                'entities' => $entities,
            ]
        );
    }

    /**
     * Admin patients dashboard.
     *
     * @Route("/entity/patient", name="admin_patient_entity_list")
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function patientsAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('AppBundle:Patient');
        $entities = $repository->findAll();

        return $this->render(
            'AppBundle:Admin/Entities/Patient:entity_table_patient.html.twig',
            [
                'entities' => $entities,
            ]
        );
    }

    /**
     * Admin users dashboard.
     *
     * @Route("/entity/user", name="admin_user_entity_list")
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function usersAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('AppBundle:User');
        $entities = $repository->findAll();

        return $this->render(
            'AppBundle:Admin/Entities/User:entity_table_user.html.twig',
            [
                'entities' => $entities,
            ]
        );
    }

    /**
     * Toggle user status.
     *
     * @Route("/toggle-user-status", name="admin_toggle_user_status", options={"expose"=true})
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function toggleUserStatusAction(Request $request)
    {
        $newStatus = $request->request->get('newStatus', null);
        $userId = $request->request->get('uid', null);

        if ($newStatus !== null && $userId !== null) {
            $em = $this->getDoctrine()->getManager();
            $repository = $em->getRepository('AppBundle:User');
            $user = $repository->find($userId);

            if (!$user) {
                throw $this->createNotFoundException(
                    $this->get('translator')->trans(
                        'No user found for id %userId%',
                        ['%userId%' => $userId]
                    )
                );
            }

            $user->setLocked(!boolval($newStatus));
            $em->flush();

            return new JsonResponse([], 200);
        }
    }
}
