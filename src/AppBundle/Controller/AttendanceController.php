<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Patient;
use AppBundle\Form\Type\AttendanceType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use AppBundle\Entity\Attendance;

/**
 * Attendance controller.
 *
 * @Route("/attendance")
 *
 * Class AttendanceController
 * @package AppBundle\Controller
 */
class AttendanceController extends Controller
{
    /**
     * Creates a new Attendance entity.
     *
     * @Route("/{id}", name="attendance_create")
     * @Method("POST")
     * @ParamConverter("patient", class="AppBundle:Patient")
     * @Security("patient.isDoctor(user) or has_role('ROLE_ADMIN')")
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param \AppBundle\Entity\Patient                 $patient
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function createAction(Request $request, Patient $patient)
    {
        $entity = new Attendance();

        // Set related patient.
        $entity->setPatient($patient);

        $form = $this->createCreateForm($entity, $patient->getId());
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            // Set flash message.
            $this->addFlash(
                'notice',
                $this->get('translator')->trans(
                    'Attendance for %firstName% %lastName% was successfully created',
                    [
                        '%firstName%' => $entity->getPatient()->getFirstName(),
                        '%lastName%' => $entity->getPatient()->getLastName(),
                    ]
                )
            );

            return $this->redirect($this->generateCrudRedirectUrl($entity));
        }

        return $this->render(
            'AppBundle:Attendance:new.html.twig',
            [
                'entity' => $entity,
                'form' => $form->createView(),
                'patient' => $patient,
                'action' => $this->generateUrl('attendance_create', ['id' => $patient->getId()]),
                'method' => 'POST',
            ]
        );
    }

    /**
     * Displays a form to create a new Attendance entity.
     * @Route("/attendance-new/{id}", name="attendance_new")
     * @Method("GET")
     * @ParamConverter("patient", class="AppBundle:Patient")
     * @Security("patient.isDoctor(user) or has_role('ROLE_ADMIN')")
     *
     * @param \AppBundle\Entity\Patient $patient
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Patient $patient)
    {
        $patientId = $patient->getId();

        $entity = new Attendance();
        $form = $this->createCreateForm($entity, $patientId);

        return $this->render(
            'AppBundle:Attendance:new.html.twig',
            [
                'method' => 'POST',
                'action' => $this->generateUrl('attendance_create', ['id' => $patientId]),
                'entity' => $entity,
                'form' => $form->createView(),
                'patient' => $patient,
            ]
        );
    }

    /**
     * Finds and displays a Attendance entity.
     *
     * @Route("/{id}", name="attendance_show")
     * @Method("GET")
     * @ParamConverter("attendance", class="AppBundle:Attendance")
     * @Security("attendance.isDoctor(user) or has_role('ROLE_ADMIN')")
     *
     * @param \AppBundle\Entity\Attendance $entity
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(Attendance $entity)
    {
        if (!$entity) {
            throw $this->createNotFoundException($this->get('translator')->trans('Unable to find Attendance entity.'));
        }

        $deleteForm = $this->createDeleteForm($entity->getId());

        return $this->render(
            'AppBundle:Attendance:show.html.twig',
            [
                'entity' => $entity,
                'delete_form' => $deleteForm->createView(),
            ]
        );
    }

    /**
     * Displays a form to edit an existing Attendance entity.
     *
     * @Route("/{id}/edit", name="attendance_edit")
     * @Method("GET")
     * @ParamConverter("attendance", class="AppBundle:Attendance")
     * @Security("attendance.isDoctor(user) or has_role('ROLE_ADMIN')")
     *
     * @param \AppBundle\Entity\Attendance $entity
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Attendance $entity)
    {
        if (!$entity) {
            throw $this->createNotFoundException($this->get('translator')->trans('Unable to find Attendance entity.'));
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($entity->getId());

        return $this->render(
            'AppBundle:Attendance:edit.html.twig',
            [
                'entity' => $entity,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            ]
        );
    }

    /**
     * Edits an existing Attendance entity.
     *
     * @Route("/{id}", name="attendance_update")
     * @Method("PUT")
     * @ParamConverter("attendance", class="AppBundle:Attendance")
     * @Security("attendance.isDoctor(user) or has_role('ROLE_ADMIN')")
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param \AppBundle\Entity\Attendance              $entity
     *
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function updateAction(Request $request, Attendance $entity)
    {
        $em = $this->getDoctrine()->getManager();

        if (!$entity) {
            throw $this->createNotFoundException($this->get('translator')->trans('Unable to find Attendance entity.'));
        }

        $deleteForm = $this->createDeleteForm($entity->getId());
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            // Set flash message.
            $this->addFlash(
                'notice',
                $this->get('translator')->trans(
                    'Attendance for %firstName% %lastName% was successfully updated',
                    [
                        '%firstName%' => $entity->getPatient()->getFirstName(),
                        '%lastName%' => $entity->getPatient()->getLastName(),
                    ]
                )
            );

            return $this->redirect($this->generateCrudRedirectUrl($entity));
        }

        return $this->render(
            'AppBundle:Attendance:edit.html.twig',
            [
                'entity' => $entity,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            ]
        );
    }

    /**
     * Deletes a Attendance entity.
     *
     * @Route("/{id}", name="attendance_delete")
     * @Method("DELETE")
     * @ParamConverter("attendance", class="AppBundle:Attendance")
     * @Security("attendance.isDoctor(user) or has_role('ROLE_ADMIN')")
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param \AppBundle\Entity\Attendance              $entity
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, Attendance $entity)
    {
        $form = $this->createDeleteForm($entity->getId());
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:Attendance')->find($entity->getId());

            if (!$entity) {
                throw $this->createNotFoundException(
                    $this->get('translator')->trans('Unable to find Attendance entity.')
                );
            }

            $em->remove($entity);
            $em->flush();

            // Set flash message.
            $this->addFlash(
                'notice',
                $this->get('translator')->trans(
                    'Attendance for %firstName% %lastName% was successfully deleted',
                    [
                        '%firstName%' => $entity->getPatient()->getFirstName(),
                        '%lastName%' => $entity->getPatient()->getLastName(),
                    ]
                )
            );
        }

        return $this->redirect($this->generateCrudRedirectUrl($entity));
    }

    /**
     * Generate CRUD redirect URL for Attendance's dashboard.
     *
     * @param \AppBundle\Entity\Attendance $attendance
     *
     * @return string
     */
    public function generateCrudRedirectUrl(Attendance $attendance)
    {
        return $this->generateUrl('patient_attendances_list', ['id' => $attendance->getPatient()->getId()]);
    }

    /**
     * Creates a form to edit a Attendance entity.
     *
     * @param Attendance $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Attendance $entity)
    {
        $form = $this->createForm(
            new AttendanceType(),
            $entity,
            [
                'action' => $this->generateUrl('attendance_update', ['id' => $entity->getId()]),
                'method' => 'PUT',
            ]
        );

        $form->add('submit', 'submit', ['label' => 'Update']);

        return $form;
    }


    /**
     * Creates a form to create a Attendance entity.
     *
     * @param Attendance $entity The entity
     * @param            $patientId
     *  Patient's ID.
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Attendance $entity, $patientId = null)
    {
        $form = $this->createForm(
            new AttendanceType(),
            $entity,
            [
                'action' => $this->generateUrl('attendance_create', ['id' => $patientId]),
                'method' => 'POST',
            ]
        );

        $form->add('submit', 'submit', ['label' => 'Create']);

        return $form;
    }

    /**
     * Creates a form to delete a Attendance entity by id.
     *
     * @param integer $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('attendance_delete', ['id' => $id]))
            ->setMethod('DELETE')
            ->add('submit', 'submit', ['label' => 'Delete', 'attr' => ['class' => 'btn btn-danger entity-del']])
            ->getForm();
    }
}
