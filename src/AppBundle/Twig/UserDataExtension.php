<?php
namespace AppBundle\Twig;

/**
 * Class TwigExtension
 * @package AppBundle\Twig
 */
class UserDataExtension extends \Twig_Extension
{
    /**
     * @return array
     */
    public function getFilters()
    {
        return array(
          'getRole' => new \Twig_Filter_Method($this, 'getRole'),
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'user_data_twig_extension';
    }

    /**
     * @param $user
     * @return mixed
     */
    public function getRole($user)
    {
        $userRoles = [];
        $readableRolesMap = [
            'ROLE_ADMIN' => 'admin',
            'ROLE_DOCTOR' => 'doctor',
            'ROLE_USER' => 'user'
        ];

        foreach ($user->getRoles() as $role) {
            if (isset($readableRolesMap[$role])) {
                $userRoles[] = $readableRolesMap[$role];
            }
        }

        return $userRoles ;
    }

}