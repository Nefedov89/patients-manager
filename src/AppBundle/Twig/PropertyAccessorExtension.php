<?php
namespace AppBundle\Twig;

use Symfony\Component\PropertyAccess\PropertyAccess;

/**
 * Class PropertyAccessorExtension
 */
class PropertyAccessorExtension extends \Twig_Extension
{
    /** @var  PropertyAccess */
    protected $accessor;


    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->accessor = PropertyAccess::createPropertyAccessor();
    }

    /**
     * @return array
     */
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('json_decode', array($this, 'jsonDecode')),
        );
    }

    /**
     * @param $value
     * @return mixed
     */
    public function jsonDecode($value)
    {
        if (empty($value)) {
            return [];
        }

        return json_decode($value, true);
    }

    /**
     * @return array
     */
    public function getFunctions()
    {
        return array(
          new \Twig_SimpleFunction('getAttribute', array($this, 'getAttribute'))
        );
    }

    /**
     * @param $entity
     * @param $property
     * @return mixed
     */
    public function getAttribute($entity, $property)
    {
        return $this->accessor->getValue($entity, $property);
    }

    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     *
     */
    public function getName()
    {
        return 'property_accessor_twig_extension';
    }
}